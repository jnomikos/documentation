---
layout: default
title: M0062 VOXL 2 Ethernet Expansion and USB Hub Datasheet
parent: M0062 VOXL 2 Ethernet Expansion and USB Hub
nav_order: 10
has_children: false
permalink: /m0062-datasheet/
---

# M0062 VOXL 2 Ethernet Expansion and USB Hub Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview
 

## Block Diagram

![m0062-diagram](/images/expansion-boards/m0062-voxl2-main-diagram.jpg)



## Dimensions

### 3D Drawings
[3D Step](https://storage.googleapis.com/modalai_public/modal_drawings/M0062_VOXL2_DEBUG_REVA.step)



## Connector Callouts

![m0062-connectors](/images/expansion-boards/m0062-datasheets.jpg)




<hr>

### J1 - Legacy Voxl Debug/Expansion Port

| Connector | MPN |
| --- | --- | 
| Board Connector | QTH-030-01-L-D-A-K-TR |
| Mating Connector | https://docs.modalai.com/voxl2-connectors/#j3---legacy-board-to-board-connector-b2b |

Color: Black


<hr>

### J2 - High Speed Board to Board Port

| Connector | MPN |
| --- | --- | 
| Board Connector | ADM6-30-01.5-L-4-2-A-TR |
| Mating Connector | https://docs.modalai.com/voxl2-connectors/#j5---high-speed-board-to-board-connector-hsb2b |

Color: Black

<hr>

### J6 - FTDI Serial DEBUG Port

| Connector | MPN |
| --- | --- | 
| Board Connector | Amphenol 10118194-0001LF |
| Mating Connector | Micro-USB Plug |


|	Pin	|	Net	|	M0062 Usage	|
|	-----	|	-------	|	-------	|
|	1	|	VBUS_DEBUG	|	VBUS	|
|	2	|	DBG_HS_CON_D_N	|	Debug Port USB_D-	|
|	3	|	DBG_HS_CON_D_P	|	Debug Port USB_D+	|
|	4	|	USB_ID	|	Not Connected (FTDI is Always Host)	|
|	5	|	DGND	|	System GND	|

<hr>

### J7 - uSD/UFS Combo Card Slot

| Connector | MPN |
| --- | --- | 
| Board Connector | Amphenol 1010170469#2A |
| Mating Products | uSD Card or UFS Card |

|	Pins	|	Nets	|	M0062 Usage	|
|	-----	|	-------	|	-------	|
|	FRONT	|	UFS, 1 Lane	|	Automatically Picked up when inserting UFS card	|
|	BACK	|	uSD (SDCC)	|	Automatically Picked up when inserting uSD card	|


<hr>

### J8 - SPI Port

| Connector | MPN |
| --- | --- | 
| Board Connector | JST SM06B-GHS-TB |
| Mating Connector | JST GHR-06V-S |

|	Pin	|	Net	|	M0062 Usage	|
|	-----	|	-------	|	-------	|
|	1	|	VREG_3P3V_LOCAL	|	3.3V Reference Voltage	|
|	2	|	SPI_MISO_3P3V_CON	|	QUP_17 SPI_MISO, 3.3V CMOS	|
|	3	|	SPI_MOSI_3P3V_CON	|	QUP_17 SPI_MOSI, 3.3V CMOS	|
|	4	|	SPI_SCLK_3P3V_CON	|	QUP_17 SPI_SCLK, 3.3V CMOS	|
|	5	|	SPI_CS0_N_3P3V_CON	|	QUP_17 SPI_CS0, 3.3V CMOS	|
|	6	|	DGND	|	System GND	|

<hr>

### J9 - 2W UART + I2C Port

| Connector | MPN |
| --- | --- | 
| Board Connector | JST SM06B-GHS-TB |
| Mating Connector | JST GHR-06V-S |

|	Pin	|	Net	|	M0062 Usage	|
|	-----	|	-------	|	-------	|
|	1	|	VREG_3P3V_LOCAL	|	3.3V Reference Voltage	|
|	2	|	UART_TX_3P3V_CON	|	QUP_19 UART TX, 3.3V CMOS	|
|	3	|	UART_RX_3P3V_CON	|	QUP_19 UART RX, 3.3V CMOS	|
|	4	|	I2C_SDA_3P3V_CON	|	QUP_10 I2C_SDA, 3.3V CMOS	|
|	5	|	I2C_SCL_3P3V_CON	|	QUP_10 I2C_SCL, 3.3V CMOS	|
|	6	|	DGND	|	System GND	|

<hr>

### J10 - USB Host Type A Port

| Connector | MPN |
| --- | --- | 
| Board Connector | Molex 0483930003 |
| Mating Connector | USB-A Plugs to USB device |

|	Pin	|	Net	|	M0062 Usage	|
|	-----	|	-------	|	-------	|
|	1	|	VBUS_PORT2	|	5V VBUS, 1A Limit	|
|	2	|	USB1_HS_CON_D_N	|	Host Port USB_D-	|
|	3	|	USB1_HS_CON_D_P	|	Host Port USB_D+	|
|	4	|	DGND	|	System GND	|
|	5	|	USB1_SS_RX_N	|	Host Port SuperSpeed USB RX- (may not work)	|
|	6	|	USB1_SS_RX_P	|	Host Port SuperSpeed USB RX+ (may not work)	|
|	7	|	DGND	|	System GND	|
|	8	|	USB1_SS_TX_CONN_N	|	Host Port SuperSpeed USB TX- (may not work)	|
|	9	|	USB1_SS_TX_CONN_P	|	Host Port SuperSpeed USB TX+ (may not work)	|
|	10	|	DGND	|	System GND	|

<hr>

### J11 - USB Host 10-Pin ModalAI Format

| Connector | MPN |
| --- | --- | 
| Board Connector | JST BM10B-GHS-TBT |
| Mating Connector | JST GHR-10V-S |

|	Pin	|	Net	|	M0062 Usage	|
|	-----	|	-------	|	-------	|
|	1	|	VBUS_PORT3	|	5V VBUS, 1A Limit	|
|	2	|	USB1_HS_CON_D_N	|	Host Port USB_D-	|
|	3	|	USB1_HS_CON_D_P	|	Host Port USB_D+	|
|	4	|	DGND	|	System GND	|
|	5	|	USB1_SS_RX_N	|	Host Port SuperSpeed USB RX-	|
|	6	|	USB1_SS_RX_P	|	Host Port SuperSpeed USB RX+	|
|	7	|	DGND	|	System GND	|
|	8	|	USB1_SS_TX_CONN_N	|	Host Port SuperSpeed USB TX-	|
|	9	|	USB1_SS_TX_CONN_P	|	Host Port SuperSpeed USB TX+	|
|	10	|	DGND	|	System GND	|

<hr>

### J12 - USB Host 4-Pin ModalAI Format

| Connector | MPN |
| --- | --- | 
| Board Connector | JST BM04B-GHS-TBT (may be blue format) |
| Mating Connector | JST GHR-04V-S |

|	Pin	|	Net	|	M0062 Usage	|
|	-----	|	-------	|	-------	|
|	1	|	VBUS_PORT4	|	5V VBUS, 1A Limit	|
|	2	|	USB1_HS_CON_D_N	|	Host Port USB_D-	|
|	3	|	USB1_HS_CON_D_P	|	Host Port USB_D+	|
|	4	|	DGND	|	System GND	|
