---
layout: default3
title: VOXL 2 USB3 UART Add-On
nav_order: 16
parent: Expansion Boards
has_children: true
permalink: /voxl2-usb3-uart-add-on/
thumbnail: /other-products/usb3-uart/capture.png
buylink: https://www.modalai.com/products/usb-3-uart-expansion-adapter
summary: This Expansion Adapter integrates USB 3 peripheral devices as well as an additional UART for VOXL 2
---

# USB3 UART Add-On
{: .no_toc }

For VOXL2 only.

<a href="https://www.modalai.com/products/usb-3-uart-expansion-adapter" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![m00125-zoom](/images/other-products/usb3-uart/capture.png)


