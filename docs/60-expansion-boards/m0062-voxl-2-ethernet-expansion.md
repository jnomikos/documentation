---
layout: default3
title: M0062 VOXL 2 Ethernet Expansion and USB Hub
nav_order: 17
parent: Expansion Boards
has_children: true
permalink: /m0062/
buylink: https://www.modalai.com/products/m0062-2
summary: M0062 VOXL 2 Ethernet Expansion and USB Hub
---

# VOXL 2 Ethernet Expansion and USB Hub

{: .no_toc }

## Overview

The M0062 VOXL 2 Ethernet Expansion and USB Hub provides convenient access to extra I/O.


<a href="https://www.modalai.com/collections/expansion-board/products/m0062-2" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![m0062-hero](/images/expansion-boards/m0062-overview.jpg/)

_Note: the USB A connector, J10, is not working consistently on this board for USB3 speeds and may only work in USB2 mode. Please use J11 if you need USB3. You can use MCBL-00022-2 to convert this into an "A-type" port._

## Details

The M0062 provides a variety of basic access to several key features supported by the VOXL 2 platform, including:
- USB3 host port in the 10-pin ModalAI format with 1A VBUS support
- USB2 host port in the 4-pin ModalAI format with 1A VBUS support
- USB2 host port in a Type-A format (this port uses a USB3 Type-A connector, but USB3 speeds may not be work, use J11)
- uSD and UFS Combo Card Socket direct on-board (accepts either uSD or UFS memory cards)
- Dedicated Linux Kernel Debug UART Port using on-board FTDI to Micro-USB
- 10/100/1000 GbE RJ-45 Ethernet Jack (with integrated magnetics)
- 6-pin JST GH for SPI port access
- 6-pin JST GH for 2W UART + I2C access