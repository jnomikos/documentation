---
layout: default
title: VOXL 2 USB3 UART Add-On User Guide
parent: VOXL 2 USB3 UART Add-On
nav_order: 15
has_children: false
permalink: /voxl2-usb3-uart-add-on-user-guide/
---

# VOXL 2 USB3 UART Add-On User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## UART Use Case

For an example use case using UART, see [VOXL 2 External Flight Controller](/voxl2-external-flight-controller/).

## USB3 Use Case

The `J2` connector on the M0125 can be used to connect a USB3 capable device. 

![m0125-standalone.jpg](/images/voxl2-usb3-uart-add-on/m0125-standalone.jpg)

### Setting up a Connection

- Using a [MCBL-00022](/cable-datasheets/#mcbl-00022) cable, connect the 10-pin JST side to the M0125's `J2` connector.

![m0125-cable.jpg](/images/voxl2-usb3-uart-add-on/m0125-cable.jpg)

- Connect your USB3 capable device to the USB connector side of the [MCBL-00022](/cable-datasheets/#mcbl-00022) cable.

![m0125-usb3.jpg](/images/voxl2-usb3-uart-add-on/m0125-usb3.jpg)

### Verifying a USB3 Capable Connection

- Connect to the VOXL2 over ADB:
```
adb shell
```

- Verify that the connected USB3 device is present:
```
lsusb -t
```

Example Output: 
```
/:  Bus 02.Port 1: Dev 1, Class=root_hub, Driver=xhci-hcd/1p, 10000M
    |__ Port 1: Dev 2, If 0, Class=Mass Storage, Driver=usb-storage, 5000M
/:  Bus 01.Port 1: Dev 1, Class=root_hub, Driver=xhci-hcd/1p, 480M
```
**NOTE** - The example output above is using a USB3.0 flash drive. USB3.0 (gen 1) devices will show 5000M (5000MBit/s) capability and USB3.1 (gen 2) devices will show 10000M (10000MBit/s) capability.

- The generation of your USB device can be verified by executing the following command and checking the `bcdUSB` line in the output:
```
# General Format: lsusb -v -s [Bus#]:[Dev#]
# Format based on example output from above:
lsusb -v -s 002:002 
```

Example Output:
```
Bus 002 Device 002: ID 090c:1000 Silicon Motion, Inc. - Taiwan (formerly Feiya Technology Corp.) Flash Drive
Device Descriptor:
  bLength                18
  bDescriptorType         1
  bcdUSB               3.10
  .
  .
  .
```