---
layout: default
title: 1. ESD Safety
parent: VOXL Developer Bootcamp
nav_order: 1
permalink: /esd-safety/
---

# ESD Safety
{: .no_toc }


---

Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware. We recommend always using grounding wrist straps and ESD mats.

<img src="/images/voxl-developer-bootcamp/m0054-esd-warning.png" alt="m0054-esd-warning"/>

### ESD issues can and have caused unexpected behaviors for developers in the past.

<br>
[Next: Hardware Quickstart](/hardware-quickstart/){: .btn .btn-green }
