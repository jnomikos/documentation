---
layout: default
title: 2.6 VOXL
parent: 2. Hardware Quickstart
nav_order: 41
has_children: false
permalink: /voxl-hardware-quickstart/
---

# VOXL Hardware Quickstart
{: .no_toc }

This section contains a hardware quickstart and is intended for developer type users.  It provides the minimum steps needed to get a device powered up and ready to connect to a console.

{: .alert .simple-alert}
**NOTE**:  *Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware.*

For technical details, see the [datasheet](/voxl-datasheet/).

## How to Setup Hardware

### How to Power On and Prepare to Connect

<br>
**NOTE**:  Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware.

- Connect Micro B side of USB cable to VOXL
- Connect other side of USB cable (recommmended USB type A) to host computer for later use

![VOXL HW Setup](/images/voxl/voxl-dk-hw-setup.png)

- Connect the power cable to J1 as shown
- Connect the other side of the power cable to the VOXL Power Module as shown
- Connect one side of the USB cable to the host computer and connect the other side to J8 as shown.
  - **Note:** the Micro USB connector will consume only a portion of the USB 3.0 port.  A USB 3.0 cable can be used as well.
- Now plug in the power supply (**5V/6A DC output**) to the wall, and plug it in to the VOXL Power Module barrel jack as shown

## How to Power Off

Pulling the power is okay in normal operation. If you are logged into VOXL and have been modifying files on the file system then it's recommended to run `sync` to make sure your changes are flushed to flash memory before pulling power.

Do not pull power while flashing software as this can brick the device.

[Next Step: Set up ADB](/setting-up-adb/){: .btn .btn-green }
