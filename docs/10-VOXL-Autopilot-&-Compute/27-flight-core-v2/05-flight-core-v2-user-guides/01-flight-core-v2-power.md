---
layout: default
title: Flight Core v2 Power
parent: Flight Core v2 User Guides
grand_parent: Flight Core v2
nav_order: 1
permalink: /flight-core-v2-power/
---

# Flight Core v2 Power
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Hardware

### J13 - VOXL Power Module v3 Input

The main power input is `J13` and is typically paired with a [VOXL Power Module v3](/power-module-v3-datasheet/), which provides two I2C based power monitoring ICs, providing PX4 the battery status.

Recommended is [MCBL-00062](/cable-datasheets/#mcbl-00062/) (Flight Core v2 to Power Module Cable) for this interface.

![Flight Core v2](/images/flight-core-v2/m0087-power.png)

### J3 - USB Power Input

New to v2 of Flight Core, is the ability to power the system off USB power (note that the main power input at `J13` takes precedence when both are present).  Note there is no power monitoring via this interface.

Recommended is [MCBL-00010](/cable-datasheets/#mcbl-00010/) (JST-to-micro-USB) for this interface, when adding your own micro USB cable.

![Flight Core v2](/images/flight-core-v2/m0087-power-usb.png)

## Software

### PX4 Driver Source

An open source PX4 driver is available at [/src/drivers/power_monitor/voxlpm](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/power_monitor/voxlpm)

### PX4 Driver Overview

(Reused from the header file, DRY!)

```
 * - VOXLPM v3 (QTY2 INA231) -
 *
 *             +~~~~~~~~~~~~~~+
 *  VBATT -----| RSENSE_VBATT | ----------+---------------------> VBATT TO ESCS
 *     |       +~~~~~~~~~~~~~~+           |
 *     |              |          +--------+------+
 *     +----+    +----+          | 5/12V REGULATOR  |
 *          |    |               +--------+------+
 *          |    |                        |   +~~~~~~~~~~~~~~+
 *          |    |                        +---| RSENSE_5VOUT |---> 5/12VDC TO COMPUTE/PERIPHERAL
 *          |    |                        |   +~~~~~~~~~~~~~~+
 *          |    |                        |         |
 *         V|    |A                      V|         |A
 *     #################              #################
 *     # INA231, 0x44  #              # INA231, 0x45  #
 *     #################              #################
 *
 *
 *     Publishes:                     Publishes:
 *     - ORB_ID(battery_status)
 *     - ORB_ID(power_monitor)        - ORB_ID(power_monitor)
 *
 ```

 The driver periodically queries two INA231 devices, and calculates the battery voltage and amperage draw (e.g. ESC power + compute power), along with the regulated 5VDC output and amperage draw (e.g. compute power).

 There are parameters that can adjust shunt resistance values if rework is done to the power module to change the fullscale range.
 