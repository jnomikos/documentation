---
layout: default
title: Flight Core v2 Telemetry
parent: Flight Core v2 User Guides
grand_parent: Flight Core v2
nav_order: 6
permalink: /flight-core-v2-telemetry/
---

# Flight Core v2 Telemetry
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Hardware

### J1 - TELEM1

UART7 is exposed on `J1`, which is mapped to `TELEM1` in PX4 and available to Nuttx as `/dev/ttyS6`.

This is the default port used by the ModalAI UART ESC Driver when enabled.  When the UART ESC is not enabled, this can be used to interface with as a typical telemetry port.

**PLEASE NOTE** in `1.13.2-0.1.1` and older, the default ESC port was J5.

| Pin # | Signal Name                                                                     |
|-------|---------------------------------------------------------------------------------|
| 1     | NC                                                                              |
| 2     | Flight Core v2 TELEM1 Rx, 5P0V                                                 |
| 2     | Flight Core v2 TELEM1 Tx, 5P0V                                                 |
| 4     | NC                                                                              |
| 5     | NC                                                                              |
| 6     | GND                                                                             |

### J5 - TELEM2

UART5 is exposed on `J5`, which is mapped to `TELEM2` in PX4 and available to Nuttx as `/dev/ttyS4`.

Recommended is [MCBL-00066](/cable-datasheets/#mcbl-00066/) (6p to 4p JST UART Cable) for this interface.

**PLEASE NOTE** in `1.13.2-0.1.1` and older, this port was used as default ModalAI ESC, which changed to J1.

![Flight Core v2](/images/flight-core-v2/m0087-telem1-v2.png)

*Flight Core v2 J1*

| Pin # | Signal Name                                                                     |
|-------|---------------------------------------------------------------------------------|
| 1     | NC                                                                              |
| 2     | Flight Core v2 Rx, ESC Tx, 3P3V                                                 |
| 2     | Flight Core v2 Tx, ESC Rx, 3P3V                                                 |
| 4     | NC                                                                              |
| 5     | NC                                                                              |
| 6     | GND                                                                             |

*VOXL 2 UART Add-On J3*

| Pin # | Signal Name                                                                     |
|-------|---------------------------------------------------------------------------------|
| 1     | NC                                                                              |
| 2     | RX, VOXL 2 `/dev/ttyHS1`                                                        |
| 2     | TX, VOXL 2 `/dev/ttyHS1`                                                        |
| 4     | GND                                                                             |
