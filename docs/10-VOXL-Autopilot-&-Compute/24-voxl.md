---
layout: default3
title: VOXL
nav_order: 24
has_children: true
permalink: /voxl/
parent: VOXL Autopilot & Compute
summary: VOXL is ModalAI's autonomous computing platform built around the Snapdragon 821.
thumbnail: /voxl/voxl-whiteb.png
buylink: https://www.modalai.com/collections/blue-uas-framework-components/products/voxl
---


# VOXL
{: .no_toc }

VOXL is ModalAI's autonomous computing platform built around the Snapdragon 821. The VOXL architecture combines a single board computer with a depth camera, flight controller and cellular modem to create fully autonomous, connected drones and robots!

<a href="https://www.modalai.com/collections/blue-uas-framework-components/products/voxl" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/9/voxl" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>

<img src="/images/voxl/voxl-whiteb.jpg" alt="voxl" width="70%">


## Why VOXL?

The VOXL technology builds on the Qualcomm® Flight Pro™ architecture by adding comprehensive software development support with a build-able Linux kernel, cross-compile SDKs, LTE, time of flight cameras, [Docker images](https://gitlab.com/voxl-public/voxl-docker) for development and more.




