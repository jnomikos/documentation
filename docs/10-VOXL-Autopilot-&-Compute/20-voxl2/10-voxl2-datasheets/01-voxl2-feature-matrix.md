---
layout: default
title: VOXL 2 Feature Matrix
parent: VOXL 2 Datasheets
nav_order: 1
permalink:  /voxl2-feature-matrix/
---

# VOXL 2 Feature Matrix
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

## Specifications

| Feature                                                                       | VOXL 2                                                           |
|-------------------------------------------------------------------------------|------------------------------------------------------------------|
| CPU                                                                           | QRB5165 <br>8 cores up to 3.091GHz <br>8GB LPDDR5<br>128GB Flash |
| OS                                                                            | Ubuntu 18.04 - Linux Kernel v4.19                                |
| GPU                                                                           | Adreno 650 GPU – 1024 ALU                                        |
| NPU                                                                           | 15 TOPS                                                          |
| Flight Controller Embedded                                                    | Yes (PX4 or Ardupilot on Sensors DSP)                            |
| Integrated Sensors                                                            | QTY. 2 ICM-42688P IMU, QTY. 1 ICP-10100                          |
| Power Consumption                                                             | 0.1 - 7W                                                         |
| Built in WiFi                                                                 | No                                                               |
| Add-on Connectivity                                                           | WiFi, 5G, 4G/LTE, Microhard                                      |
| Video Encoding                                                                | 8K30 h.264/h.265 64MP still images                              |
| Computer Vision Sensors                                                       | QTY. 2 Stereo Pair<br>QTY. 1 Tracking                            |
| Dimensions                                                                    | 70mm x 36mm                                                      |
| Weight                                                                        | 16g                                                              |
| VOXL SDK: GPS-denied navigation, SLAM, obstacle avoidance, object recognition | Yes                                                              |
| ROS                                                                           | ROS 1 & 2                                                        |
| QGroundControl                                                                | Yes                                                              |
| ATAK                                                                          | Yes                                                              |
| NDAA ’20 Section 848 Compliant                                                | Yes, Assembled in USA                                            |
| PMD TOF                                                                       | Yes                                                              |
| FLIR Boson                                                                    | USB, MIPI                                                        |
| FLIR Lepton                                                                   | USB, SPI                                                         |

