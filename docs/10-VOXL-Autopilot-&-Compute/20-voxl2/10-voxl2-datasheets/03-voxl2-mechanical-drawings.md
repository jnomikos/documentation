---
layout: default
title: VOXL 2 Mechanical Drawings
parent: VOXL 2 Datasheets
nav_order: 2
permalink:  /voxl2-mechanical-drawings/
youtubeId: xmqI3msjqdo
---

# VOXL 2 Mechanical Drawings
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# Mechanical Drawings

## 2D Drawings

### VOXL 2 IMU Locations

![voxl2-imu-locations](../../images/voxl2/m0054_voxl2_imu_locations_with_axis.png)

### Dimensions

#### Front

![voxl2-2D-front](../../images/voxl2/M0054-2D-front-dimensions.png)

#### Back

![voxl2-2D-back](../../images/voxl2/M0054-2D-back-dimensions.png)

## 3D Drawings

[3D STEP File](https://storage.googleapis.com/modalai_public/modal_drawings/M0054_VOXL2_PVT_SIP_REVA.step)
