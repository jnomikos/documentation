---
layout: default
nav_order: 30
title: VOXL 2 HW Versions
parent: VOXL 2 Datasheets
permalink: /m0054-versions/
---

# VOXL 2 HW Versions
{: .no_toc }

---

## M0054-1 and M0054-2

Two variants of VOXL2 M0054 hardware have shipped, which each require:

- a unique kernel
- a unique trustzone config

The SDK installer should detect and load the correct version.  If not, the user is given a link to this page for help to decide which hardware they have!

### Hardware Differences

You can distinguish between `M0054-1` and `M0054-2` hardware based upon the SIP package text.

| HW Version | VOXL SDK Requirement | Notes   |
|------------|----------------------|---------|
| M0054-1    | Any                  | "QRB5165M" printed on SIP cover (see below) |
| M0054-2    | SDK 1.1.3+           | "QSM8250" printed on SIP cover (see below) |

<img src="/images/voxl2/m0054-versions.jpg"/>

### Software Differences

The `voxl-version` utility will report the `machine` and `variant`.

M0054-1 will show:

```
hw_platform: M0054
mach.var:    1.0
```

M0054-2 will show:

```
hw_platform: M0054
mach.var:    1.2
```

This is populated by the [voxl-platform-mod](https://gitlab.com/voxl-public/system-image-build/meta-voxl2/-/tree/qrb5165-ubun1.0-14.1a/recipes-kernel/voxl-platform-mod) kernel driver.

| Machine  | Variant   | Description                         | modalai,platform-id (machine, variant, pad)|
|----------|-----------|-------------------------------------|---------------------|
| m0054    |           | VOXL2                               | 1 0 0               |
| m0054    | var01     | VOXL2 - no combo mode J6/J8         | 1 1 0               |
| m0054    | var02     | VOXL2 - 8250                        | 1 2 0               |

See `/sys/module/voxl_platform_mod/parameters/machine` and `/sys/module/voxl_platform_mod/parameters/variant`

See [Release Notes](/voxl2-voxl2-mini-system-image/).