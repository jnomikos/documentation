---
layout: default
title: M0015 Stereo Module Datasheet
parent: Image Sensors
nav_order: 15
has_children: false
permalink: /M0015/
---

# VOXL Stereo Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MSU-M0015picture.jpg](/images/other-products/image-sensors/MSU-M0015picture.jpg)

Stereo VGA monochrome (grayscale) sensor kit that plugs directly into VOXL® and VOXL Flight. With 2 cameras, and some computer vision processing, a depth map can be generated. With a depth map and location, obstacle avoidance can be achieved. This configuration is ideal for outdoor SLAM (Simultaneous Location and Mapping) applications. Uses OV7251 global-shutter, VGA imaging sensors.




## Specification

### MSU-M0015-1-01 OV7251 85° FOV ([Buy Here](https://www.modalai.com/products/msu-m0015))


| Specification | Value                                                                                |
|----------------|--------------------------------------------------------------------------------------|
| Sensor         | OV7251 [Datasheet](https://www.ovt.com/wp-content/uploads/2022/01/OV7251-PB-v1.9-WEB.pdf)|
| Shutter        | Global, Hardware-Synchronized                                                        |
| Resolution     | 1280x480 (640x480 * 2)                                                               |
| Framerate      | up to 60Hz                                                                           |
| Data formats   | YUV only                                                                             |
| Lens Size      | 1/7.5"                                                                               |
| Focusing Range | 5cm~infinity                                                                         |
| Focal Length   | 1.77mm                                                                               |
| F Number       | 2.5                                                                                  |
| Fov(DxHxV)     | 85° x 68° x 56°                                                                      |
| TV Distortion  | < -3.5%                                                                              |
| Weight         | 4g                                                                                   |


### MSU-M0113-1 

| Specicifcation | Value                                                                                |
|----------------|--------------------------------------------------------------------------------------|
| Sensor         | OV9782                                                                               |


### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File
[M0015-1 Found Here](https://www.modalai.com/products/msu-m0015)

#### 2D Diagram
[M0015-1 Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0015_2D_11-02-21.pdf)

## Module Connector Pinout 
### Left

| Pin # | Signal Name            | Pin # | Signal Name            |
|-------|------------------------|-------|------------------------|
| 1     | GND                    | 2     | GND                    |
| 3     | VREG_L23A_2P8          | 4     | CAM_MCLK1_BUFF         |
| 5     | GND                    | 6     | GND                    |
| 7     | NC                     | 8     | CAM1_RST0_N            |
| 9     | VREG_S4A_1P8           | 10    | L_FLASH                |
| 11    | GND                    | 12    | GND                    |
| 13    | CCI_I2C_SCL0           | 14    | MIPI_CSI1_LANE0_CONN_P |
| 15    | CCI_I2C_SDA0           | 16    | MIPI_CSI1_LANE0_CONN_M |
| 17    | GND                    | 18    | GND                    |
| 19    | NC                     | 20    | MIPI_CSI1_CLK_CONN_P   |
| 21    | CAM_SYNC_0             | 22    | MIPI_CSI1_CLK_CONN_M   |
| 23    | L_ULPM                 | 24    | GND                    |

### Right

| Pin # | Signal Name            | Pin # | Signal Name            |
|-------|------------------------|-------|------------------------|
| 1     | GND                    | 2     | GND                    |
| 3     | VREG_L23A_2P8          | 4     | CAM1_MCLK3             |
| 5     | GND                    | 6     | GND                    |
| 7     | NC                     | 8     | CAM_RST1_N             |
| 9     | VREG_S4A_1P8           | 10    | R_FLASH                |
| 11    | GND                    | 12    | GND                    |
| 13    | CCI_I2C_SCL1           | 14    | MIPI_CSI1_LANE2_CONN_P |
| 15    | CCI_I2C_SDA0           | 16    | MIPI_CSI1_LANE2_CONN_M |
| 17    | GND                    | 18    | GND                    |
| 19    | NC                     | 20    | MIPI_CSI1_LANE3_CONN_P |
| 21    | CAM_SYNC_0             | 22    | MIPI_CSI1_LANE3_CONN_M |
| 23    | R_ULPM                 | 24    | GND                    |

[Pinout Images](https://storage.googleapis.com/modalai_public/modal_drawings/M0015_pinoutImages.pdf)
[Module Connector Schematic](https://storage.googleapis.com/modalai_public/modal_drawings/M0015_moduleConnectorSchematics.pdf)



## VOXL 2 Integration
SDK 0.7 and greater

More information [here](/voxl2-camera-configs/)

| Cable              | Adapter Requirements                                      |
|--------------------|-----------------------------------------------------------|
| [M0008-1](/M0008/) | [M00076](/M0076/) or [M0084](/M0084/) or [M0135](/M0135/) |
| [M0010-1](/M0010/) | [M00076](/M0076/) or [M0084](/M0084/) or [M0135](/M0135/) |
| [M0109-1](/M0109/) | None                                                      |


## VOXL SDK
### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor
### Indoor
Stereo-front left
{:target="_blank"}
![stereo_front_l_in.png](/images/other-products/image-sensors/Samples/stereo_front_l_in.png)
{:target="_blank"}
Stereo-front right
{:target="_blank"}
![stereo_front_r_in.png](/images/other-products/image-sensors/Samples/stereo_front_r_in.png)

### Outdoor
Stereo-front left
{:target="_blank"}
![stereo_front_l_out.png](/images/other-products/image-sensors/Samples/stereo_front_l_out.png)
{:target="_blank"}
Stereo-front right 
{:target="_blank"}
![stereo_front_r_out.png](/images/other-products/image-sensors/Samples/stereo_front_r_out.png)

## Hardware Design Guidance