---
layout: default
title: M0014 Tracking Module Datasheet
parent: Image Sensors
nav_order: 14
has_children: false
permalink: /M0014/
---

# VOXL Tracking Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MSU-M0014picture.jpg](/images/other-products/image-sensors/MSU-M0014picture.jpg)


Tracking VGA sensor and flex cable to connect to VOXL® and VOXL 2 for indoor flight, GPS-denied navigation VIO and global shutter computer vision processing.

This module is based on an OV7251 VGA, global-shutter CMOS sensor with fisheye lens.




## Specification

### MSU-M0014-1-01 OV7251 166° FOV ([Buy Here](https://www.modalai.com/products/m0014))


| Specification  | Value                                                                                |
|----------------|--------------------------------------------------------------------------------------|
| Sensor         | OV7251 [Datasheet](https://www.ovt.com/wp-content/uploads/2022/01/OV7251-PB-v1.9-WEB.pdf) |
| Shutter        | Global                                                                               |
| Resolution     | 640x480                                                                              |
| Framerate      | 30,60,90Hz implemented on VOXL, sensor suports up to 120Hz                           |
| Data formats   | B&W 8 and 10-bit                                                                     |
| Lens Size      | 1/3.06"                                                                              |
| Focusing Range | 5cm~infinity                                                                         |
| Focal Length   | 0.83mm                                                                               |
| F Number       | 2.0                                                                                  |
| Fov(DxHxV)     | 166° x 133° x 100°                                                                   |
| TV Distortion  | -20.77%                                                                              |

### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0014%20Tracking%20Camera.STEP)

#### 2D Diagram
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0014_2D_11-02-21.pdf)

## Module Connector Pinout

| Pin # | Signal Name            | Pin # | Signal Name            |
|-------|------------------------|-------|------------------------|
| 1     | GND                    | 2     | GND                    |
| 3     | NC                     | 4     | NC                     |
| 5     | SDA                    | 6     | D0VDD1.8V              |
| 7     | SCL                    | 8     | NC                     |
| 9     | RST_0                  | 10    | XCLK                   |
| 11    | GND                    | 12    | GND                    |
| 13    | MCP                    | 14    | STROBE                 |
| 15    | MCP                    | 16    | NC                     |
| 17    | MDP                    | 18    | NC                     |
| 19    | MDN                    | 20    | AVDD2.8V               |
| 21    | GND                    | 22    | GND                    |
| 23    | NC                     | 24    | NC                     |
| 25    | NC                     | 26    | FSIN                   |
| 27    | NC                     | 28    | NC                     |
| 29    | NC                     | 30    | NC                     |
| 31    | GND                    | 32    | GND                    |
| 33    | NC                     | 34    | NC                     |
| 35    | NC                     | 36    | GND                    |

[Pinout Images](https://storage.googleapis.com/modalai_public/modal_drawings/M0014_pinoutImages.pdf)


## VOXL Integration

## VOXL SDK
### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor
### Indoor
![tracking_in.png](/images/other-products/image-sensors/Samples/tracking_in.png)
{:target="_blank"}

### Outdoor
![tracking_out.png](/images/other-products/image-sensors/Samples/tracking_out.png)
{:target="_blank"}

## Hardware Design Guidance

