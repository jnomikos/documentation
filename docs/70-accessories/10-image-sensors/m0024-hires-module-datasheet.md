---
layout: default
title: M0024 IMX214 Module Datasheet
parent: Image Sensors
nav_order: 24
has_children: false
permalink: /M0024/
---

# VOXL Hi-res Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MSU-M0024picture.jpg](/images/other-products/image-sensors/MSU-M0024picture.jpg)


## Specification

### M0024-1 10cm IMX214 107° FOV  ([Buy Here](https://www.modalai.com/products/msu-m0024-1))

| Specification | Value                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------|
| Sensor         | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter        | Rolling                                                                                             |
| Resolution     | 4208x3120                                                                                           |
| Framerate      | up to 60Hz                                                                                          |
| Lens Size      | 1/3.06"                                                                                             |
| Focusing Range | 5cm~infinity                                                                                        |
| Focal Length   | 3.33mm                                                                                              |
| F Number       | 2.75                                                                                                |
| Fov(DxHxV)     | 107°                                                                                                |
| TV Distortion  | < 6%                                                                                                |
| Weight         | 3.78g                                                                                               |

### M0024-2 10cm IMX214 146° FOV  ([Buy Here](https://www.modalai.com/products/msu-m0024-1))

| Specification | Value                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------|
| Sensor         | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter        | Rolling                                                                                             |
| Resolution     | 4208x3120                                                                                           |
| Framerate      | up to 60Hz                                                                                          |
| Lens Size      | 1/3.06in                                                                                            |
| Focal Length   | 2.65MM                                                                                              |
| F Number       | 2.0 +/- 5%                                                                                          |
| Fov(DxHxV)     | 143° x 106° x 77°                                                                                   |
| TV Distortion  | < 15%                                                                                               |
| Weight         | 2.95g                                                                                               |

### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File


#### 2D Diagram
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0024_2D_11-01-21.pdf)

## Module Connector Pinout for J2

| Pin # | Signal Name            | Pin # | Signal Name            |
|-------|------------------------|-------|------------------------|
| 1     | GND                    | 2     | GND                    |
| 3     | NC                     | 4     | NC                     |
| 5     | CCI_I2C_SDA0           | 6     | VREG_LVS1A_1P8         |
| 7     | CCI_I2C_SCL0           | 8     | VREG_L3A_1P1           |
| 9     | CAM0_RST0_N            | 10    | CAM_MCLK0_BUFF         |
| 11    | GND                    | 12    | GND                    |
| 13    | MIPI_CSI0_CLK_CONN_P   | 14    | CAM_FLASH              |
| 15    | MIPI_CSI0_CLK_CONN_M   | 16    | CAM_SYNC_0             |
| 17    | MIPI_CSI0_LANE0_CONN_P | 18    | NC                     |
| 19    | MIPI_CSI0_LANE0_CONN_M | 20    | VREG_L22A_2P8          |
| 21    | GND                    | 22    | GND                    |
| 23    | MIPI_CSI0_LANE1_CONN_P | 24    | NC                     |
| 25    | MIPI_CSI0_LANE1_CONN_M | 26    | NC                     |
| 27    | MIPI_CSI0_LANE2_CONN_P | 28    | NC                     |
| 29    | MIPI_CSI0_LANE2_CONN_M | 30    | NC                     |
| 31    | GND                    | 32    | GND                    |
| 33    | MIPI_CSI0_LANE3_CONN_P | 34    | NC                     |
| 35    | MIPI_CSI0_LANE3_CONN_M | 36    | GND                    |

[Module Connector Schematic](https://storage.googleapis.com/modalai_public/modal_drawings/J2moduleConnectorSchematic.pdf)

## VOXL Integration

## VOXL SDK
### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor
### Indoor
### Outdoor

## Hardware Design Guidance

