---
layout: default
title: M0040 Time of Flight Module Datasheet
parent: Image Sensors
nav_order: 40
has_children: false
permalink: /M0040/
---
# VOXL Hires Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MSU-M0040picture.jpg](/images/other-products/image-sensors/MSU-M0040picture.jpg)

This sensor module is EOL, please refer to the current [VOXL 2 Time of Flight Module](/M0178/)

The PMD Time of Flight (ToF) sensor produces high-fidelity depth mapping indoors up to 6m. When used with the VOXL platform, this sensor is mutually exclusive to the stereo cameras, meaning the stereo cameras need to be replaced with the TOF Add-on.






## Specification

### MSU-M0040-1-01 ([Buy Here](https://www.modalai.com/products/voxl-dk-tof))


| Specification    | Value                                                                                                                                                                    |
|-------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Part Number       | MSU-M0040-1-01                                                                                                                                                           |
| Technology        | [PMD](https://www.pmdtec.com/)                                                                                                                                           |
| Rate              | 5 - 45FPS in configurable option modes for distance / accuracy / framerate                                                                                               |
| Exposure Time     | 4.8 ms typ. @ 45 fps / 30 ms typ. @ 5 fps                                                                                                                                |
| Resolution        | 224 x 171 (38k) px                                                                                                                                                       |
| FOV (H x W)       | 85.1° 106.5° (measured by experiment)                                                                                                                                    |
| Range             | 4 - 6m                                                                                                                                                                   |
| Illumination      | 850nm                                                                                                                                                                    |
| Depth Resolution  | <= 1% of distance (0.5 – 4m @ 5fps) <= 2% of distance (0.1 – 1m @ 45fps)                                                                                                 |
| Time Sync         | No physical pin, but the frame timestamp is measured with 50ns precision on a single clock. All of the sensors on the VOXL platform are timestamped for computer vision. |
| Power Consumption | <2W                                                                                                                                                                      |
| Weight            | 3g                                                                                                                                                                       |
| Dimensions        | 24mm x 10.6mm                                                                                                                                                            |
| Eye Safe          | Yes                                                                                                                                                                      |


### Requirements
* VOXL (APQ8096) Requires VOXL System Image 2.3 or greater.
* VOXL 2 (QRB5165) Requires SDK 0.9 or greater

### Current/Power Consumption

## Technical Drawings   

#### 3D File
[Download STEP](https://storage.googleapis.com/modalai_public/modal_drawings/MCAM-00005-TOF-A65.STEP)

[Download SolidWorks](https://storage.googleapis.com/modalai_public/modal_drawings/MCAM-00005-TOF-A65.SLDPRT)

#### 2D Diagram


## Module Connector Pinout J1 MFPC-M0040
#### Connector Specs

| VOXL Board Connector                                                                                          | M0169-1 J? Mating Connector                                                                                  |
|---------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| Panasonic, [MPN: AXT336124](https://www3.panasonic.biz/ac/ae/control/connector/base-fpc/p4s/index.jsp?c=move) | [Panasonic MPN: AXT436124](https://www3.panasonic.biz/ac/ae/control/connector/base-fpc/p4s/index.jsp?c=move) |

#### Pin-out

| Pin # | Signal Name            | Pin # | Signal Name            |
|-------|------------------------|-------|------------------------|
| 1     | GND                    | 2     | GND                    |
| 3     | VREG_L17A_2P8 (AFVDD)  | 4     | CAM0_STANDBY_N         |
| 5     | CCI_I2C_SDA0           | 6     | VREG_LVS1A_1P8 (DOVDD) |
| 7     | CCI_I2C_SCL0           | 8     | VREG_L3A_1P1 (DVDD)    |
| 9     | CAM0_RST0_N            | 10    | CAM_MCLK0_BUFF         |
| 11    | GND                    | 12    | GND                    |
| 13    | MIPI_CSI0_CLK_CONN_P   | 14    | CAM_FLASH              |
| 15    | MIPI_CSI0_CLK_CONN_M   | 16    | CAM_SYNC_0             |
| 17    | MIPI_CSI0_LANE0_CONN_P | 18    | CAM0_MCLK3             |
| 19    | MIPI_CSI0_LANE0_CONN_M | 20    | VREG_L22A_2P8 (AVDD)   |
| 21    | GND                    | 22    | GND                    |
| 23    | MIPI_CSI0_LANE1_CONN_P | 24    | CAM_RST1_N             |
| 25    | MIPI_CSI0_LANE1_CONN_M | 26    | CAM_SYNC_1             |
| 27    | MIPI_CSI0_LANE2_CONN_P | 28    | CCI_I2C_SDA1           |
| 29    | MIPI_CSI0_LANE2_CONN_M | 30    | CCI_I2C_SCL1           |
| 31    | GND                    | 32    | GND                    |
| 33    | MIPI_CSI0_LANE3_CONN_P | 34    | VPH_PWR                |
| 35    | MIPI_CSI0_LANE3_CONN_M | 36    | GND                    |
| 35    | NC                     | 36    | GND                    |

[Pinout Images](https://storage.googleapis.com/modalai_public/modal_drawings/M0040pinoutImages.pdf)

## VOXL Integration

## VOXL SDK
Note: This guide assumes that you are able to run commands on VOXL2; if you cannot, please see [VOXL Developer Bootcamp](https://docs.modalai.com/voxl-developer-bootcamp/). All commands specified below should be run on VOXL2. 

### Supported Sensors
- M0040-1 (Legacy TOF)

### M0040-1 Camera Server Configuration

In order to set up your VOXL2 to use the M0040-1 ToF sensor, connect the camera in one of the ToF configurations described on the [VOXL2 Camera Configs](https://docs.modalai.com/voxl2-camera-configs/) page. Then, run `voxl-configure-cameras <id>`, where `<id>` is the ID number of the configuration you selected. Reboot VOXL2, and then camera server should be publishing frames from the ToF camera. To verify functionality, please see the [VOXL Portal section](#VOXL-Portal) below.

### VOXL Portal

In order to verify the functionality of your ToF configuration, you can attempt to view the IR and depth map images in VOXL portal. In order to view VOXL portal, your VOXL should be connected to the internet (see [VOXL2 WiFi Setup](https://docs.modalai.com/voxl-2-wifi-setup/)). 

After your VOXL is connected to the internet, run `systemctl status voxl-portal`. Then, find the IP address of your VOXL using `hostname -I` and visit this address in the web browser of a computer connected to the same local network as your VOXL (or your VOXL's access point, in AP mode). Then, navigate to "Cameras > TOF Conf / Depth / IR" in the top navigation bar, and verify that the displayed video stream matches what the camera is pointed at. 


## Image Samples for Sensor
### Indoor
### Outdoor

## Hardware Design Guidance