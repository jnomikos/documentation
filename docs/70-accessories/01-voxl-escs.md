---
layout: default3
title: VOXL ESCs
nav_order: 1
has_children: true
parent: Accessories
permalink: /voxl-escs/
thumbnail: /modal-esc/esc.png
buylink: https://www.modalai.com/products/voxl-esc
summary: Documentation for ModalAI's high-performance, closed-loop Electronic Speed Controllers (ESCs) that use a digital interface (UART, i2c in future).
---

# ModalAI Electronic Speed Controller (ESC)
{: .no_toc }

ModalAI's Electronic Speed Controllers (ESCs) are high-performing, closed-loop speed controllers that use a digital interface (UART, i2c in future).

<a href="https://www.modalai.com/products/voxl-esc" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/13/escs" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>

## Brief Overview
Brushless Electronic Speed Controllers (**ESCs**) are devices that consist of hardware
and software for controlling three-phase brushless DC (**BLDC**) motors. ESCs communicate
with the flight controller, which instructs the ESCs how fast the motor should spin.

ModalAI's ESCs implement the following advanced features:
* Full integration with ModalAI VOXL and Flight Core PCBs
* Bi-directional UART communication with checksum; status and fault monitoring
* Real-time status and health reporting at high update rate (100Hz+ each)
* Closed-loop RPM control for best flight performance
* LED control from flight controller via UART

## Feature Comparison

<table>
<tr>
  <td><center><b>M0129</b></center></td>
  <td><center><b>M0134</b></center></td>
  <td><center><b>M0138</b></center></td>
</tr>
<tr>
  <td><center><img src="/images/modal-esc/m0129/m0129_top_bottom.jpg" width="250"></center></td>
  <td><center><img src="/images/modal-esc/m0134/m0134_top_bottom.jpg" width="260"></center></td>
  <td><center><img src="/images/modal-esc/m0138/m0138_top_bottom.jpg" width="380"></center></td>
</tr>
</table>

| Feature                                                                                                 | ModalAI 4-in-1 ESC (M0129)                  | ModalAI 4-in-1 ESC (M0134)                  | ModalAI 4-in-1 ESC (M0138)                  |
|---------------------------------------------------------------------------------------------------------|---------------------------------------------|---------------------------------------------|---------------------------------------------|
| Nominal Input Voltage                                                                                   | 6.0V-16.8V (2-4S Lipo)                      | 6.0V-16.8V (2-4S Lipo)                      | 6.0V-26V (2-6S Lipo)                        |
|                                                                                                         |                                             | 6.0V-25.2V (2-6S Lipo) (M0134-6)            |                                             |
| VOXL Power Output                                                                                       | 3.8V or 5.0V @ 5A (6A 30 sec)               | ❌                                          | 5.0V or 3.8V @ 6A                           |
| Aux Power Output                                                                                        | 1x 3.3V / 5.0V 500mA                        | 1x 5.0V              500mA                  | 1x 3.3V/5.0V 500mA, 1x 16.8V 500mA          |
| Max Continuous Current Per Motor                                                                        | 20A (thermally limited, requires airflow)   | 20A (thermally limited, requires airflow)   | 40A (thermally limited, requires airflow)   |
| Max Burst Current Per Motor                                                                             | 30-40A for <1s                              | 40-50A for <1s                              | 100A for <1s                                |
| MCU                                                                                                     | STM32F051K86                                | STM32F051K86                                | STM32F051K86                                |
| MOSFET Driver                                                                                           | MP6531A                                     | MP6530 (M0134-1)                            | MP6531A                                     |
|                                                                                                         |                                             | MP6531A (M0134-3, M0134-6)                  |                                             |
| MOSFETs                                                                                                 | AON7528 (N)                                 | AON7528 (N)                                 | TBD                                         |
| Current Sensing                                                                                         | 1x 0.5mOhm + INA186 (-20A..+110A)           | 4x 0.5mOhm + INA186 (-10A..+55A)            | 1x 0.25mOhm + INA186 (-35A..+200A)          |
| ESD signal protection                                                                                   | ✅                                          | ✅                                          | ✅                                          |
| Temperature Sensing                                                                                     | ✅ (internal to MCU)                        | ✅ (internal to MCU)                        | ✅ (internal to MCU + top and bottom)       |
| On-board Status LEDs                                                                                    | ✅ 4x                                       | ✅ 4x                                       | ✅ 4x                                       |
| External LEDs                                                                                           | 1x Neopixel LED output                      | 2x Neopixel LED outputs                     | 1x Neopixel LED output                      |
| Secure Bootloader                                                                                       | Yes (AES256)                                | Yes (AES256)                                | Yes (AES256)                                |
| Motor PWM Switching Frequency                                                                           | 48, 24 Khz                                  | 48, 24 Khz                                  | 48, 24 Khz                                  |
| Maximum RPM (6 pole pairs)                                                                              | 50K+                                        | 50K+                                        | 50K+                                        |
| PWM control input (1-2ms)                                                                               | ✅                                          | ✅                                          | ❌                                          |
| PWM output                                                                                              | ✅ 4x (shared with PWM input)               | ✅ 4x (shared with PWM input)               | ✅ 2x                                       |
| Active Freewheeling                                                                                     | ✅                                          | ✅                                          | ✅                                          |
| Disable Regenerative Braking                                                                            | Always enabled                              | Always enabled (M0134-1)                    | ✅ Option to disable                        |
|                                                                                                         |                                             | ✅ Option to disable (M0134-3, M0134-6)     |                                             |
| Tone Generation                                                                                         | ✅                                          | ✅                                          | ✅                                          |
| Closed-loop RPM Control                                                                                 | Yes (10Khz)                                 | Yes (10Khz)                                 | Yes (10Khz)                                 |
| Number of UART ports                                                                                    | 2 (2Mbit+)                                  | 2 (2Mbit+)                                  | 1 (2Mbit+)                                  |
| [voxl_esc](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/actuators/voxl_esc) UART Protocol | ✅                                          | ✅                                          | ✅                                          |
| UART connector type                                                                                     | JST GH 4-pin                                | Hirose DF13 6-pin                           | JST GH 4-pin                                |
| UART Cable (for VOXL2)                                                                                  | mcbl-00015                                  | mcbl-00029                                  | mcbl-00015                                  |
| Weight without wires (g)                                                                                | 5.9                                         | 9.5                                         | 17.9                                        |
| Board Dimensions (mm)                                                                                   | 36.5 x 36.5                                 | 40.5 x 40.5                                 | 45.5 x 59.0                                 |
| Mounting Hole Size, Pattern (mm)                                                                        | 2.5, 30.5 x 30.5                            | 3.05, 31.0 x 33.0                           | 3.05, 30.5 x 30.5                           |
| Notes                                                                                                   |                                             |                                             |                                             |

## Mechanical Drawings

| PCB     | 3D STEP                                                                                                              |
|---------|----------------------------------------------------------------------------------------------------------------------|
| M0049 | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0049_ESC_4_IN_1_REVB_CAM_FINAL_20200610.stp) |
| M0117 | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0117_ESC_4_IN_1_AD_REVA(-1_MAIN).step)       |
| M0134 | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0134_ESC_4_IN_1_32_REVA.step)                |
| M0129 | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0129_ESC_4_IN_1_MICRO_20230222_FINAL.stp)    |
| M0138 | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0138_RACING_ESC_4_IN_1_REVA.step)    |


## Other ESCs

<table>
<tr>
  <td><center>M0049<br><img src="/images/modal-esc/m0049/m0049_top_bottom.jpg" width="300"></center></td>
  <td><center>M0117<br><img src="/images/modal-esc/m0117/m0117_top_bottom.jpg" width="300"></center></td>
</tr>
</table>

| Feature                                                                                                 | ModalAI 4-in-1 ESC (M0049)                  | ModalAI 4-in-1 ESC (M0117)                  |
|---------------------------------------------------------------------------------------------------------|---------------------------------------------|---------------------------------------------|
| Nominal Input Voltage                                                                                   | 6.0V-16.8V (2-4S Lipo)                      | 6.0V-16.8V (2-4S Lipo)                      |
|                                                                                                         |                                             |                                             |
| Aux Power Output                                                                                        | 1x 4.5V, 1x 5.0V (adj) 600mA each           | 1x 5.0V (adjustable) 600mA                  |
| Max Continuous Current Per Motor                                                                        | 20A (thermally limited)                     | 20A (thermally limited)                     |
| Max Burst Current Per Motor                                                                             | 40-50A (requires airflow and heat-spreader) | 40-50A (requires airflow and heat-spreader) |
| MCU                                                                                                     | STM32F051K86                                | STM32F051C6U6                               |
| MOSFET Driver                                                                                           | MP6530                                      | MP6530 (M0117-1)                            |
|                                                                                                         |                                             | MP6531A (M0117-3)                           |
| MOSFETs                                                                                                 | AON7528 (N)                                 | AON7528 (N)                                 |
| Individual Current Sensing                                                                              | 4x 1mOhm + INA186                           | 4x 0.5mOhm + INA186                         |
| ESD signal protection                                                                                   | ✅                                          | ✅                                          |
| Temperature Sensing                                                                                     | ✅                                          | ✅                                          |
| On-board Status LEDs                                                                                    | ✅                                          | ✅                                          |
| External LEDs                                                                                           | Neopixel LEDs                               | N/A                                         |
| Secure Bootloader                                                                                       | Yes (AES256)                                | Yes (AES256)                                |
| PWM Switching Frequency                                                                                 | 48, 24 Khz                                  | 48, 24 Khz                                  |
| Maximum RPM (6 pole pairs)                                                                              | 50K+                                        | 50K+                                        |
| PWM control input                                                                                       | ✅                                          | ✅                                          |
| Active Freewheeling                                                                                     | ✅                                          | ✅                                          |
| Disable Regenerative Braking                                                                            | ❌ Always enabled                           | ❌ Always enabled (M0117-1)                 |
|                                                                                                         |                                             | ✅ (M0117-3)                                |
| Tone Generation                                                                                         | ✅                                          | ✅                                          |
| Closed-loop RPM Control                                                                                 | Yes (10Khz)                                 | Yes (10Khz)                                 |
| Number of UART ports                                                                                    | 2 (2Mbit+)                                  | 2 (2Mbit+)                                  |
| [modal_io](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modal_io) UART Protocol | ✅                                          | ✅                                          |
| Weight without wires (g)                                                                                | 9.5                                         | 9.5                                         |
| Board Dimensions (mm)                                                                                   | 40.5 x 40.5                                 | 40.5 x 40.5                                 |
| Mounting Hole Size, Pattern (mm)                                                                        | 3.05, 31.0 x 33.0                           | 3.05, 31.0 x 33.0                           |
| Notes                                                                                                   | limited availability                        | limited availability                        |
