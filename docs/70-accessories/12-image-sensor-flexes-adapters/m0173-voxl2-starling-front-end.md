---
layout: default
title: M0173 VOXL 2 Starling Front End
parent: Image Sensor Flex Cables and Adapters
nav_order: 173
has_children: false
nav_exclude: false
permalink: /M0173/
---

# M0173 VOXL 2 Starling 2 Front End
{: .no_toc }

M0173 Serves as a breakout for Camera Groups J6 and J7 on VOXL 2. It exposes 4 COAX camera connectors for use with M0166 Tracking Cameras or M0161 IMX412 Hires cameras. It also exposes a connector for use with a single PMD TOF sensor, and a connector for attaching an M0157 Lepton plus rangefinder board.

[VOXL 2 Coax Camera Bundle](/voxl2-coax-camera-bundles/)

---
## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


### VOXL SDK Requirements

VOXL SDK 1.3.0+ is required.

### Hardware Requirements

Only compatible with [VOXL 2](/voxl2/)


## VOXL2 Driven Image Sensor Synchronization

In the D0014/D0012 architecture using the M0173 breakout, the following occurs for the tracking sensors:

- The [voxl-fysnc-mod](https://gitlab.com/voxl-public/system-image-build/meta-voxl2/-/tree/qrb5165-ubun1.0-14.1a/recipes-kernel/voxl-fysnc-mod) ships in SDK 1.3+ and defaults to use GPIO 109, 30Hz, and disabled.
- When a C26/C27/C8 image sensor configurations is setup using `voxl-configure-cameras`, the config file at `/etc/modalai/voxl-camera-server.conf` is setup with:

```
...
"fsync_en":	true,
"fsync_gpio":	109,
...
```

- When camera server runs, it will use the `voxl-fsync-mod` params exposed to the file system at `/sys/module/voxl_fsync_mod/parameters`, for example `enabled`, `gpio_num`, and enable the sync pulse at the default 30Hz.
- When a C26/C27/C8 image sensor configurations is setup using `voxl-configure-cameras`, the `AR0144` drivers are copied into `/usr/lib/camera` that are setup for slave mode and will capture sychronized frames at the rate defined (note: the FPS should also be configured in `/etc/modalai/voxl-camera-server.conf` accordinly to ensure proper register settings are loaded)

[![D0014-M0173.jpg](/images/d0014/D0014-M0173.jpg)](/images/d0014/D0014-M0173.jpg)

## Downward Range Finder and FLIR Lepton

In the D0014/D0012 architecture using the M0173 breakout:

- VL53L1CX Range Finder is accesible over `/dev/i2c-4`, see `/etc/modalai/voxl-rangefinder-server.conf` and `voxl-rangefinder`
- Not supported in SDK 1.3.0 but coming soon, FLIR Lepton is accesible over `/dev/i2c-4` and `/dev/spidev0.0`, see `/etc/modalai/voxl-lepton-server.conf` and `voxl-lepton-server`

[![D0014-M0157.jpg](/images/d0014/D0014-M0157.jpg)](/images/d0014/D0014-M0157.jpg)


## New Kernel Configuration for M0173

The `D0014/D0012` drone family uses the new kernel `config 1` (as opposed to `config 0`).  See `/sys/module/voxl_platform_mod/parameters/config` to verify proper kernel config.
