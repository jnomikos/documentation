---
layout: default
title: Starling 2
nav_order: 31
has_children: true
permalink: /starling-2/
parent: Dev Drones
summary: Starling 2 is our flagship development drone for both indoor and outdoor flight.
thumbnail: /starling-v2/starling-v2-hero-1.png
buylink: https://www.modalai.com/starling-2
---

# Starling 2
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

Starling 2 is our flagship development drone for indoor flight.

### Video Overview


### Starling Running voxl-mapper



