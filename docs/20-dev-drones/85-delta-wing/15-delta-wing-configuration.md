---
layout: default
title: Delta Wing Configuration
parent: Delta Wing
nav_order: 05
has_children: false
permalink: /delta-wing-configuration/
---

# Delta Build Configuration

By SDK 1.4.0 release, D0015 (Delta Wing) will be part of the configuration of MPA, so the user will be able to run `voxl-configure-sku d0015` & `voxl-configure-mpa` and ignore all the steps below.

The following is a step by step guide as to how to update the VOXL 2 and all required IO in order to fly the delta wing using the VOXL 2 via voxl-px4 and a microhard connection.

In order to setup voxl-px4 to run fixed wing - proceed to boot the VOXL 2 and either ADB shell into the board, or ssh into it via the microhard. once inside the linux environment - the user can now run the following to configure PX4:

```
voxl-configure-px4 d0015`
```

This will configure PX4 to start the required drivers in order to fly the fixed wing in position flight with the correct airspeed sensor, distance sensor, communication methods, etc.

Once voxl-px4 has been setup - we can move over to camera server where we will have both an imx412 and a tracking camera (agnostic of OV7251 vs AR0144) in J7 upper and lower - configure camera server for this combination via the custom camera script in /data/modalai/CUSTOM CAMERA CONFIG FILE and place the correct camera in the correct locations and save the file. Proceed to then run `voxl-configure-cameras C` and then you should be good to go from the camera side!

At this point now we can configure the other services which will not be as manual:

```
1. voxl-cpu-monitor: proceed to run `voxl-configure-cpu-monitor enable`
2. voxl-vision-hub: proceed to run `voxl-configure-vision-hub factory_enable`
3. voxl-imu-server: proceed to run `voxl-configure-imu-server` and select imu_apps as the main reference
4. voxl-streamer: proceed to run `voxl-configure-streamer enable`
5. voxl-mavcam-manager: proceed to run `voxl-configure-mavcam-manager enable`
6. voxl-qvio-server: proceed to run `voxl-configure-qvio-server enable` - this one will be more manual as you will need to first calibrate your tracking camera
7. voxl-mavlink-server: proceed to run `voxl-configure-mavlink-server enable`
```

Once the above have been configured, proceed to do a hard reboot on your VOXL 2 and all the IO. The next time you boot the system you should then be able to connect to the VOXL 2's instance of PX4 via QGC and update the parameters with the preset delta wing parameters provided here: ***********INSERT LINK TO DELTA WING PARAMS**********

Now once that is done, proceed to do one last hard reboot on the entire delta wing and on the next reboot, you should be off to the races capable of flying position based flight with the delta wing and streaming video back to QGC over the microhard! It is suggested to wire up a traditional RC controller over USB to your QGC instance so it is easier to fly as fixed wings arent traditionally flown with spring loaded controllers.