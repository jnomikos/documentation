---
layout: default
title: Delta Wing Unboxing
parent: Delta Wing
nav_order: 05
has_children: false
permalink: /delta-wing-unboxing/
---

# Delta Wing Unboxing

The SoloGood Delta Wing is a styrafoam ABS delta wing with the following dimensions:

```
Wingspan: 69 cm
Height: 8.8 cm
Length: 41.5 cm
```

The body space on the delta wing includes:

```
Battery compartment: 175mm*75mm*55mm
ESC compartment: 70mrn*38mm*20mm
Camera: 20mm*21mm*30mm
```

Maximum flying speed is <27km/h with a endurance range of 70 minutes depending on battery/motor combination. Takeoff weight post build with all components ranges from 350g - 750g.

The delta wing comes un-built in the following packaging:

![unboxed_delta_wing.jpg](/images/delta-wing/delta_wing_components.jpg)

Unboxed - it has the following components:

![unboxed_delta_wing.jpg](/images/delta-wing/unboxed_delta_wing.jpg)

The kit does NOT include motors, esc's, batteries, flight controllers, or servos. Furthermore the following is recommended to drive flight:

```
Recommend Power: 2205/2350kv
Recommend Battery: 3-4s like 3-4S 1550 -2200MAH / 6S 1300-1550MAH
Recommend :ESC: 35A
3S 6inch Biblades / 4S 5inch Biblades
```

Purchasing of the delta wing can be found here: https://tinyurl.com/4a43f37x