---
layout: default
title: Sentinel Spektrum Bind Board
parent: Sentinel Datasheets
nav_order: 21
has_children: False
permalink: /sentinel-spektrum-bind-board/
---

# Sentinel Spektrum Bind Board
{: .no_toc }

## Overview

The Sentinel Spektrum Bind Board is a hardware solution that allows software binding of a Spektrum radio receiver directly to your RB5 Flight or Voxl2 (Sentinel) based drone solution. It creates a seamless software experience for binding without all the hassle of a binding plug or other plug removal/insertion methods.

This accesory is specific to Spektrum and will not work with other R/C receivers.

## Block Diagram

This diagram shows both options for RB5 Flight and Sentinel installation options.

![M0094-BD](/images/other-products/cables/m0094-diagram.png)

## Pin Tables

In both options, the 3-position JST SHR connector matches the Spektrum pinout as follows:

|	Pin	|	Net	|	Notes/Usage	|
|	-----	|	-------	|	-------	|
|	1	|	VREG_3V3_RC	|	Spektrum Power	|
|	2	|	GND	|	Spektrum GND	|
|	3	|	RC_UART_RX	|	Spektrum UART TX	|

The reason for the different configurations with M0094 illustrated here is that the RB5 Flight platform has a dedicated GNSS/Mag connector (J19), whereas Voxl2 (Sentinel) has a multi-purpose connector J19 that has GNSS/Mag + R/C added, so the cable assembly is a little more complicated.


# RB5 Configuration

The RB5 configuration mates with J12 and J10 simultaneously.

J10 Mate: "P3" 8-pin JST GHR plug 

|	Pin	|	Net	|	M0094 Connection	|
|	-----	|	-------	|	-------	|
|	1	|	VREG_3V3	|	J1 Pin 4	|
|	2	|	NC	|	No Connect	|
|	3	|	NC	|	No Connect	|
|	4	|	NC	|	No Connect	|
|	5	|	NC	|	No Connect	|
|	6	|	GPIO_46	|	J1 Pin 5	|
|	7	|	NC	|	No Connect	|
|	8	|	NC	|	No Connect	|

J12 Mate: "P1" 4-pin JST GHR plug

|	Pin	|	Net	|	M0094 Connection	|
|	-----	|	-------	|	-------	|
|	1	|	VREG_3V3_RC	|	J1 Pin 1	|
|	2	|	NC	|	No Connect	|
|	3	|	RC_UART_RX	|	J1 Pin 3	|
|	4	|	GND	|	J1 Pin 6	|


# Sentinel (Voxl2) Configuration

The Sentinel configuration mates with J19 and J10 simultaneously.

J10 Mate: "P3" 8-pin JST GHR plug 

|	Pin	|	Net	|	M0094 Connection	|
|	-----	|	-------	|	-------	|
|	1	|	VREG_3V3	|	J1 Pin 4	|
|	2	|	NC	|	No Connect	|
|	3	|	NC	|	No Connect	|
|	4	|	NC	|	No Connect	|
|	5	|	NC	|	No Connect	|
|	6	|	GPIO_46	|	J1 Pin 5	|
|	7	|	NC	|	No Connect	|
|	8	|	NC	|	No Connect	|

J19 Mate: "P1" 12-pin JST GHR plug

|	Pin	|	Net	|	M0094 Connection	|
|	-----	|	-------	|	-------	|
|	1	|	VDC_5V_LOCAL	|	No M0094, Pass to GNSS Module	|
|	2	|	GNSS_UART_TX	|	No M0094, Pass to GNSS Module	|
|	3	|	GNSS_UART_RX	|	No M0094, Pass to GNSS Module	|
|	4	|	MAG_SCL	|	No M0094, Pass to GNSS Module	|
|	5	|	MAG_SCDA	|	No M0094, Pass to GNSS Module	|
|	6	|	GND	|	Pass to GNSS Module	|
|	7	|	NC	|	No Connect	|
|	8	|	NC	|	No Connect	|
|	9	|	VREG_3V3_RC	|	J1 Pin 1	|
|	10	|	NC	|	No Connect	|
|	11	|	RC_UART_RX	|	J1 Pin 3	|
|	12	|	GND	|	J1 Pin 6	|


