---
layout: default
title: VOXL m500 First Flight
parent: M500 User Guides
nav_order: 25
has_children: false
permalink: /voxl-m500-user-guide-first-flight/
---

1. TOC
{:toc}

# Sentinel First Flight
{: .no_toc }

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*


## Preflight Checks

Now disconnect the 5V power supply or battery you used during the configuration steps. Move the M500 to a safe location where you wish to fly and connect a battery for flight while the M500 is on the ground.

The position and direction VOXL powers up in will dictate the origin and orientation of the Visual Inertial Odometry (VIO) coordinate frame, also known as LOCAL_POSITION_NED in PX4 terminology. VOXL will correct for any tilt due to non-level ground.


### Attitude Check

Lift the vehicle and move it around, verify that the attitude reported in QGroundControl GUI looks and responds correctly. Try not to cover the tracking camera during this process.

![qgc_attitude_gui.png](/images/voxl-sdk/qgc/qgc_attitude_gui.png)

### VIO Data Validation

We recommend your first flight be in position mode. See [Here](/voxl-m500-user-guide-using-vio/) for an overview.
Make sure to validate VIO data before flying in position mode. 

### Arming Vehicle Without Props

With the propellers **off** arm the vehicle.

- Set killswitch to off ("Aux2 Gov" switch down)
- **Left** stick hold **down** and to the **right**

Validate the motor rotation is as expected and the vehicle responds to throttling up momentarily.

Disarm the vehicle:

- **Left** stick hold **down** and to the **left**

Correct rotation direction of motors:

![QuadRotorX.svg](/images/voxl-sdk/qgc/QuadRotorX-small.svg)

### Install Propellers

Install the propellers following this orientation. Each motor shaft has a white or black top to indicate which propeller goes where. Note that the clockwise spinning propellers are reverse threaded to prevent loosening during flight.

Ensure you tighten the propellers down tightly by hand or they may spin off when engaging the kill switch.

![vehicle-overview-props-144-1024.jpg](/images/m500/vehicle-overview-props-144-1024.jpg)


### Safety Check
Before flying, make sure to conduct a thorough safety check: 
<ul>
<li>Make sure all connectors and cabling is secure and not loose </li>
<li>Make sure none of the wires are cut, frayed, or damaged </li>
<li>If you have any connectivity add-ons, make sure the antennas are secure and are not loose </li>
<li>Make sure the SD and SIM cards have not popped out </li>
<li>Make sure the reciever is properly plugged in </li>
<li>Make sure the cameras are free of any grease, dust, or debris </li>
<li>Make sure the battery you are using is fully  charged </li>
<li>Make sure the props are on correctly and that they are securly attached </li>
</ul>

### First Flight (Position Mode)

You should be comfortable flying before proceeding! 

We recommend your first flight be in position hold (vio) mode outside with a GPS lock. 
For more information on flying in position mode, click the green button below for a vio overview.

Once confortable with position mode, now safely fly in manual mode! No instructions here, this is an advanced mode and you should know what you are doing if you're flying!


[Next Step: Using VIO](/voxl-m500-user-guide-using-vio/){: .btn .btn-green }