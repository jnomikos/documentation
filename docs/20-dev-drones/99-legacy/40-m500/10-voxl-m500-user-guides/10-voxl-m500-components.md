---
layout: default
title: VOXL m500 Components
parent: M500 User Guides
nav_order: 5
has_children: false
permalink: /voxl-m500-components/
---

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

### Vehicle Components

The following images give an overview of the vehicle components.

![vehicle-overview-front-144-1024.jpg](/images/m500/vehicle-overview-front-144-1024.jpg)

![vehicle-overview-back-zoom-144-1024.jpg](/images/m500/vehicle-overview-back-zoom-144-1024.jpg)

![vehicle-overview-right-144-1024.jpg](/images/m500/vehicle-overview-right-144-1024.jpg)

![vehicle-overview-left-144-1024.jpg](/images/m500/vehicle-overview-left-144-1024.jpg)

[Next Step: Connect Network](/voxl-m500-user-guide-connect-network/){: .btn .btn-green }



