---
layout: default
title: Seeker User Guide
parent: Seeker
nav_order: 5
has_children: true
permalink: /seeker-user-guide/
---

# Seeker Reference Drone User Guide
{: .no_toc }

This guide will walk you from taking the Seeker out of the box and up into the air!

For technical details, see the [datasheet](/seeker-datasheet/).

1. TOC
{:toc}

--- 

[First Step: Unboxing](/seeker-user-guide-unboxing/){: .btn .btn-green }
