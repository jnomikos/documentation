---
layout: default3
title: M500
nav_order: 40
has_children: true
permalink: /m500/
parent: Legacy EOL Dev Drones
buylink: https://www.modalai.com/products/voxl-m500
summary: VOXL m500 is a built around a VOXL Flight Deck configured and tested for GPS-based autonomous flight and indoor GPS-denied navigation.
thumbnail: /m500/m500.png
---

# M500
{: .no_toc }

The M500 is an A fully assembled and calibrated Development Drone for PX4 GPS-Denied Navigation and Obstacle Avoidance. It is Pre-configured for GPS-denied navigation using Visual Inertial Odometry (VIO).

<a href="https://www.modalai.com/products/voxl-m500" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/6/voxl-m500-reference-drone" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![voxl-m500](/images/m500/m500-shopify.jpg)

