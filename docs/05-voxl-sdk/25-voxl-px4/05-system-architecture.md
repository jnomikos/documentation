---
title: System Architecture
layout: default
parent: VOXL PX4
has_children: true
nav_order: 05
permalink: /voxl-px4-system-architecture/
---

# Introduction

The QRB5165 based build of PX4 for SDK release 1.0.0 is based on the 1.14.0 version
of PX4 but has some custom features that allow it to work on the QRB5165. Mostly these
changes were required due to the underlying architecture of the QRB5165 device
and it's software architecture. The QRB5165 device is produced by Qualcomm, Inc. and
documentation about it's feature set can be found on the Qualcomm site.
[This](https://docs.qualcomm.com/bundle/publicresource/topics/80-PV086-1/introduction.html) provides a nice overview.

# Architecture

The QRB5165 device is a powerful System-on-Chip (SoC) that combines a number of
high powered processing cores and compute accelerators. The main applications processor
is composed of a complex of eight ARM64 cores running Linux. There is also a DSP based on the Qualcomm
Hexagon architecture used for sensors applications. It is on this sensors DSP where the
majority of the PX4 flight controller software runs. This DSP is often referred to
as the SDSP, or SLPI in various documentation. It runs the QuRT RTOS which is a custom
and proprietary Qualcomm product. Some of the PX4 flight controller also runs on the
Linux application processor. So essentially PX4 runs as a split architecture on QRB5165
with all flight critical real time processing occurring on the DSP and support tasks
such as logging and Mavlink processing occurring on the application processor. Understanding
this split architecture is one of the keys to understanding how the QRB5165 based 
implementation of PX4 differs from PX4 on other architectures. How this affects
development is explained in more detail in the development guide.

## I/O

A major part of any flight controller is the support for external sensors and other
components. The QRB5165 provides an assortment of IO options for standard external
sensors such as an IMU, barometer, GPS, magnetometer, radio control, and ESC. It is
important to understand what IO options are available when designing a custom drone
or outfitting a ModalAI development drone with new peripherals. 
- It is important to note that VOXL2 has no native support for PWM or CAN. Add on boards
are required to get those types of I/O ports

