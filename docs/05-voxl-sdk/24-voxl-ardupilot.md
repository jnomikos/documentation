---
title: VOXL ArduPilot 
layout: default
parent: VOXL SDK
has_children: false
nav_order: 24
permalink: /voxl-ardupilot/
---

# VOXL ArduPilot

ModalAI doesn't officially support ArduPilot running directly on VOXL 2 yet, but initial support
for the board is in mainline ArduPilot now. This page will explain current status and will
be updated as improvements are made.

For advanced / experienced users who see the VOXL 2 support in ArduPilot and want
to start playing around with it here are some helpful tips.

So far it has only been tested on two different models of small quadcopter. No
fixed wing (e.g. arduplane) has happened yet although we are making plans to begin
that integration soon (including the voxl2_io board for PWM outputs).

Only one Mavlink stream has been setup. This is for the ground control station
stream. The onboard stream which is required for ModalAI applications like VIO
and Obstacle Avoidance is not yet available.

- Start with the newest SDK (1.3.0+)
- Update the modalai-slpi image to the [latest dev version](http://voxl-packages.modalai.com/dists/qrb5165/dev/binary-arm64/modalai-slpi_1.1.19-202407112016_arm64.deb)
- Update voxl-mavlink-server to the [latest](http://voxl-packages.modalai.com/dists/qrb5165/dev/binary-arm64/voxl-mavlink-server_1.4.2-202407171720_arm64.deb)
- Install the [new libslpi-link package](http://voxl-packages.modalai.com/dists/qrb5165/dev/binary-arm64/libslpi-link_1.0.0-202406251056_arm64.deb)

You need to setup a directory for ArduPilot to use for parameter files, scripts, logs, etc.
on the VOXL 2 and give it the correct permissions to allow the DSP to access it. Running ardupilot
the first time will create the directory. So you can run ardupilot, then reboot to get the
directory created or just create it yourself before if you want to populate a default
parameters file (e.g. /data/APM/default.parm)

- ```cd /data```
- ```mkdir APM```
- ```chown system:system APM```

There are a couple of default parameter files available in mainline right now
[here](https://github.com/ArduPilot/ardupilot/tree/master/Tools/Frame_params/ModalAI)
The AutonomyDevKit parameters should also be good for the Starling 2

All of that will be done automatically once we have a Debian package and proper installer ready.

The base build does not have an orientation specified for the IMU, nor an external
magnetomer. Those need to be supplied via the appropriate parameters. Also, the
motor mapping needs to be properly configured via parameters.

You can get the firmware builds directly from the ArduPilot servers or you can
build it yourself. In order to build it you will need to procure the Hexagon SDK
from Qualcomm and integrate into a build docker. [This project](https://gitlab.com/voxl-public/voxl-docker-images/voxl-docker-ardupilot-build) will help get that all setup.

There is more setup info on the [ArduPilot repo](https://github.com/ArduPilot/ardupilot/tree/master/libraries/AP_HAL_QURT/ap_host/service)

Here is a forum post with [more information](https://forum.modalai.com/topic/3640/ardupilot-on-voxl2)

Test flight video [here](https://www.youtube.com/watch?v=l6c65-E-lzg)


