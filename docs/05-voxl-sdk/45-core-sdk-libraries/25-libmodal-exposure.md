---
title: libmodal_exposure
layout: default
parent: Core SDK Libraries
has_children: false
nav_order: 25
permalink: /libmodal-exposure/
---


# libmodal-exposure
{ | .no_toc }

libmodal_exposure a histogram to an image (with metadata) and returns what the new exposure/gain should be.

Configuration of libmodal_exposure is achieved through [voxl-camera-server.conf](https://docs.modalai.com/voxl-camera-server/#camera-server-config-file)

The parameters exposed include the following

| Parameter            | Description                                                                                                                                                                                                                                                                                                                    |
|----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `ae_desired_msv`     | the desired mean sample value, a.k.a. the average value of pixels that the auto exposure algorithm should try to achieve in frame                                                                                                                                                                                              |
| `ae_k_p_ns`          | the desired p_ns for the exposure algorithm                                                                                                                                                                                                                                                                                    |
| `ae_k_i_ns`          | the desired k_i for the exposure algorithm                                                                                                                                                                                                                                                                                     |
| `ae_max_i`           | the desired max_i for the exposure algorithm                                                                                                                                                                                                                                                                                   |
| `ae_filter_alpha`    | a low-pass filter constant that filters the calculated MSV to slow down responses - the filter used is an IIR filter                                                                                                                                                                                                           |
| `ae_ignore_fraction` | maximum percentage of saturated (255) pixels that will be used in calculation of MSV. If there are more saturated_pixels / total_pixels, then additional saturated pixels are not used to calculate MSV. This helps prevent image getting too dark if there are large blobs of very bright light                               |
| `ae_slope`           | ratio that specifies how much gain vs exposure should be changed when trying to achieve desired MSV. Both gain and exposure linearly affect the pixel brightness, but gain and exposure have different effects on the image quality - mostly in the sense that gain affects granularity and that exposure affects motion blur. |
| `ae_exposure_period` | controls the duration where the cells of the camera sensor are exposed to light                                                                                                                                                                                                                                                |
| `ae_gain_period`     | the gain period associated to the auto exposure (think of this as an amplification factor of the pixels)                                                                                                                                                                                                                       |


Source code and README on [Gitlab](https://gitlab.com/voxl-public/core-libs/libmodal_exposure)