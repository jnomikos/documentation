---
title: Building ROS Nodes
layout: default
parent: Custom ROS Applications
nav_order: 12
permalink: /building-ros-nodes/
---


# Building ROS Nodes
{: .no_toc }

---

## Overview

VOXL ships with the [`voxl-utils`](https://gitlab.com/voxl-public/voxl-utils) and [`voxl-nodes`](https://gitlab.com/voxl-public/voxl-nodes) package pre-installed alongside ROS Indigo so you can start developing with ROS right away.

You can choose to compile ROS nodes either directly on-target (on VOXL), or in the `voxl-emulator` docker on a host PC and then push the compiled application onto the VOXL (see [voxl-emulator](/voxl-emulator/) for more info). 

## Building on VOXL

1) Prerequisite: you will need to have [`ros-indigo-build-deps`](https://storage.googleapis.com/modalai_public/modal_packages/latest/ros-indigo-build-deps_1.0.0.ipk) installed to build ROS nodes directly on VOXL. This is not necessary when building in the `voxl-emulator` docker image.

2) ADB or ssh into VOXL and go to your VOXL's home directory. This assumes you have followed the previous setup pages in this manual to get ADB and/or ssh working.

```bash
me@mylaptop:~$ adb shell

...

voxl2:/$ cd ~/
voxl2:~$ 
```

3) make a new catkin workspace and initialize it with catkin_make

```bash
voxl2:~$ mkdir -p catkin_ws/src
voxl2:~$ cd catkin_ws
voxl2:~/catkin_ws$  catkin_make
```

4) Make a new node source directory in the src folder. Here we use the VOXL time-of-flight node as an example.

```bash
voxl2:~/catkin_ws$ cd src
voxl2:~/catkin_ws/src$ git clone https://gitlab.com/voxl-public/tof_cam_ros.git
voxl2:~/catkin_ws/src$ ls
CMakeLists.txt  tof_cam_ros
voxl2:~/catkin_ws/src$ cd ../
voxl2:~/catkin_ws$
```

5) Build!

```bash
voxl2:~/catkin_ws$ catkin_make install
```

6) This installed the newly built ROS node to `~/catkin_ws/install` which should be part of your ROS_PACKAGE_PATH if you have `voxl-utils` installed and it is sourced by the `~/my_ros_env.sh` script. Check to confirm

```bash
voxl2:~/catkin_ws$ exec bash
voxl2:~/catkin_ws$ env | grep "ROS_PACKAGE_PATH"
ROS_PACKAGE_PATH=/home/root/catkin_ws/install/share:/home/root/catkin_ws/install/stacks:/opt/ros/indigo/share:/opt/ros/indigo/stacks
```

7) Now you can launch your new ROS node! **Note** the tof_cam_ros node will fail to start unless you have a TOF sensor installed on VOXL.

```bash
voxl2:~/catkin_ws$ cd ../
voxl2:~$ rospack list | grep tof_cam_ros
tof_cam_ros /home/root/catkin_ws/install/share/tof_cam_ros

voxl2:~$ roslaunch tof_cam_ros tof.launch
```

## Building in `voxl-emulator` Docker Image

You can build a ROS node in the voxl-emulator docker image following the same steps as you did for building on the VOXL itself. However, the process of getting the built package onto the VOXL itself will be different.

The `voxl-nodes` package and it's README file provide a great example of how to do this. We recommend copying this project and swapping out the nodes in `catkin_ws/src` for your own and working from there. It includes instructions on how to build and helper scripts for making a package and installing on VOXL.

<https://gitlab.com/voxl-public/voxl-nodes>
