---
title: Enabling File Encryption
layout: default
parent: Misc. User Guides
has_children: true
nav_order: 20
permalink: /enabling-file-encryption/
---


# Background
VOXL2 supports hardware-based file encryption which can be enabled without any changes in the Kernel. `Fscrypt` software is used to encrypt folders and files.
- each top-level encrypted folder uses a separete encryption key
- sub-directories, file contents, file names, and symbolic links are encrypted
- timestamps, file sizes, number of files, and extended attributes are not encrypted

# Setup
- Check which device is mounted as the root file system (`/`). In this case, it is `/dev/sda9`

```
voxl2:/$ mount
/dev/sda9 on / type ext4 (rw,relatime)
...
```
- Confirm that the file system uses page-sized blocks (both should be 4096):

```
voxl2:/$ tune2fs -l /dev/sda9 | grep "Block size"
Block size:               4096

voxl2:/$ getconf PAGE_SIZE
4096
```

- Enable encryption on the device that corresponds to the desired (`/`) partition

```
voxl2:/$ tune2fs -O encrypt,stable_inodes /dev/sda9
tune2fs 1.44.3 (10-July-2018)
```

- Set up fscrypt tool

```
fscrypt setup
```
- Check `fscrypt` status

```
voxl2:~/$ fscrypt status
filesystems supporting encryption: 1
filesystems with fscrypt metadata: 1

MOUNTPOINT     DEVICE     FILESYSTEM  ENCRYPTION     FSCRYPT
/              /dev/sda9  ext4        supported      Yes
...
```


# Usage
- Create an empty directory

```
mkdir /home/root/encrypted_folder
```
- Encrypt new directory
   - The directory has to be created first, encrypted and then files and sub-directories that are added will be automatically encrypted

```
voxl2:~$ fscrypt encrypt /home/root/encrypted_folder/
Should we create a new protector? [y/N] y
The following protector sources are available:
1 - Your login passphrase (pam_passphrase)
2 - A custom passphrase (custom_passphrase)
3 - A raw 256-bit key (raw_key)
Enter the source number for the new protector [2 - custom_passphrase]: 2
Enter a name for the new protector: my_new_protector
Enter custom passphrase for protector "my_new_protector": 
Confirm passphrase: 
"/home/root/encrypted_folder/" is now encrypted, unlocked, and ready for use.
```

- Check status of the encrypted directory

```
voxl2:~$ fscrypt status /home/root/encrypted_folder/
"/home/root/encrypted_folder/" is encrypted with fscrypt.

Policy:   e33179f4c202cd56b8bf5bd173299072
Options:  padding:32 contents:AES_256_XTS filenames:AES_256_CTS policy_version:2 
Unlocked: Yes

Protected with 1 protector:
PROTECTOR         LINKED  DESCRIPTION
6d14bd87c1bad978  No      custom protector "my_new_protector"

```
- Create a test file in the encrypted (unlocked) directory

```
voxl2:~$ cd encrypted_folder/
voxl2:~/encrypted_folder$ echo "encryption test" > test.txt
voxl2:~/encrypted_folder$ more test.txt 
encryption test
```
- Reboot VOXL2 or lock the folder

```
voxl2:~/encrypted_folder$ ls
test.txt
voxl2:~/encrypted_folder$ cd ../
voxl2:~$ fscrypt lock encrypted_folder 
"encrypted_folder" is now locked.
voxl2:~$ cd encrypted_folder/
voxl2:~/encrypted_folder$ ls
AAAAAAAAAAAFH2ySNPYji272kseHB,V930yZHVIT5RlMUU,rY3PLoA
```
- Unlock the encrypted folder

```
voxl2:~/encrypted_folder$ fscrypt unlock .
Enter custom passphrase for protector "my_new_protector": 
"." is now unlocked and ready for use.
voxl2:~/encrypted_folder$ ls
test.txt
voxl2:~/encrypted_folder$ more test.txt 
encryption test
```
