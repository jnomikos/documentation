---
layout: default
title: OpenCL 0.9
parent: 3rd Party Libs 0.9
search_exclude: true
nav_order: 50
permalink: /build-for-gpu-opencl-0_9/
---

# Build for GPU (OpenCL)
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


## Overview

VOXL contains an embedded [Adreno 530](https://en.wikipedia.org/wiki/Adreno) GPU with 256 ALUs. This GPU is exposed through both OpenCL 1.1 and OpenGLES 3.1. The GPU can be exploited for significant algorithmic acceleration for use cases like computer vision and deep learning.


## Examples


voxl-dfs-server
voxl-tflite-server