---
layout: default
title: Setup ROS on VOXL 2 0.9
parent: ROS 0.9
search_exclude: true
nav_order: 11
has_children: false
permalink: /setup-ros-on-voxl-2-0_9/
youtubeId: zEGe-5EdXC0
---

# Setup ROS on VOXL 2

*ModalAI Top Tip:* ROS is not required to use or develop with VOXL 2.

If you like ROS, let's proceed:

---

## Tutorial Video 

{% include youtubePlayer.html id=page.youtubeId %}

## Install ROS

VOXL 2 / RB5 Flight can use apt package management, which allows flexibility on what ROS version to install, and allows us to keep the core system image smaller.  For these reasons, we leave it up to the end user to select what to install.

To install ROS Melodic (as an example), connect VOXL 2 / RB5 Flight to the internet and run the following:

```
apt-get update
apt install -y ros-melodic-ros-base ros-melodic-image-transport
```

## MPA

If you have not already configured MPA, please [check this out](/voxl-configure-mpa/) first as it is a pre-requisite.

As a shortcut, if you have a VOXL 2 Sentinel:

```bash
voxl-configure-mpa -p -f sentinel
```

If you have an RB5 Flight:

```bash
voxl-configure-mpa -p -f rb5-flight
```

## Launch 

Source ROS setup script:

```
voxl2:~$ . /opt/ros/melodic/setup.sh
```

Run the [voxl_mpa_to_ros](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros) node, which uses this launch file `/opt/ros/melodic/share/voxl_mpa_to_ros/launch/voxl_mpa_to_ros.launch`

```
voxl2:~$ roslaunch voxl_mpa_to_ros voxl_mpa_to_ros.launch
```

You should get similar output:

```
... logging to /home/root/.ros/log/81ccbc5c-34ac-11ed-80c6-00c0caafca29/roslaunch-m0054-2535.log
Checking log directory for disk usage. This may take a while.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://m0054:45955/

SUMMARY
========

PARAMETERS
 * /rosdistro: melodic
 * /rosversion: 1.14.13

NODES
  /
    voxl_mpa_to_ros_node (voxl_mpa_to_ros/voxl_mpa_to_ros_node)

auto-starting new master
process[master]: started with pid [2557]
ROS_MASTER_URI=http://localhost:11311

setting /run_id to 81ccbc5c-34ac-11ed-80c6-00c0caafca29
process[rosout-1]: started with pid [2568]
started core service [/rosout]
process[voxl_mpa_to_ros_node-2]: started with pid [2571]


MPA to ROS app is now running

Found new interface: hires
Found new interface: qvio_overlay
Found new interface: stereo_front
Found new interface: stereo_rear
Found new interface: tracking
Found new interface: imu_apps
Found new interface: voa_pc_out
Found new interface: qvio
```

## List Topics

In a new shell, run the following:

```
voxl2:~$ rostopic list
/hires
/imu_apps
/qvio/odometry
/qvio/pose
/qvio_overlay
/rosout
/rosout_agg
/stereo_front/left
/stereo_front/right
/stereo_rear/left
/stereo_rear/right
/tracking
/voa_pc_out
```

## Next Steps

See the [MAVROS test example for VOXL 2](/mavros-voxl-2/).

See the [building ROS nodes for VOXL](/build-ros-nodes-for-voxl/) for information on making your own ROS nodes.

