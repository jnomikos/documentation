---
layout: default
title: UVC Camera Supprt 0.9
parent: High Level Features 0.9
search_exclude: true
nav_order: 55
permalink: /voxl-uvc-server-0_9/
---

# UVC Camera Support
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

voxl-uvc-server is an application to send video frames from UVC cameras to Modal Pipe Architecture (MPA) pipes.
This server is often used in conjunction with [voxl-streamer](https://docs.modalai.com/voxl-streamer/) but
can be used with any application that desires frames from a UVC camera.

There are a large array of available UVC cameras on the market and it is impossible
to say that voxl-uvc-server supports all of them. In general, voxl-uvc-server supports
UVC cameras that have raw frame output. It has been specifically tested with:
* Logitech C270
* Logitech C930e
* Logitech Brio (USB 2.0 and USB 3.0 on VOXL 2)
* Videology 5MP Micro USB 2.0 Board Camera
* [PureThermal Mini with Flir Lepton](https://groupgets.com/manufacturers/getlab/products/purethermal-mini-flir-lepton-smart-i-o-module)
* Flir Boson 640
* Blue Robotics h264 camera

It has also been tested in conjunction with voxl-streamer using the following viewers on Ubuntu 20.04:
* QGroundControl v4.1.4
* QGroundControl v3.5.6
* VLC 3.0.16

voxl-uvc-streamer will connect with the first UVC camera that is found that meets
the specified configuration. It can only support streaming from a single UVC camera
at a time.

## Installation

voxl-uvc-server is installed via ipk package. It is currently only available as a dev package.
```
yocto:/home/root/ipk# voxl-configure-opkg dev
yocto:/home/root/ipk# opkg update
yocto:/home/root/ipk# opkg install voxl-uvc-server
```

## Running voxl-uvc-server

voxl-uvc-server is a command line application and can be started on the command line:
```
yocto:/home/root# voxl-uvc-server
```

Use the ```-h``` option to view online help:
```
yocto:/home/root# voxl-uvc-server -h
Usage: voxl-uvc-server <options>
Options:
-d                Show extra debug messages
-m                Show extra MPA debug messages
-v <vendor-id>    USB vendor id of the desired UVC device in hexadecimal (e.g. 090c)
                  Default is search for any vendor id
-p <product-id>   USB product id of the desired UVC device in hexadecimal (e.g. 337b)
                  Default is search for any product id
-r <resolution>   Desired image resolution (e.g. 320x240)
                  Default is 640x480
-f <fps>          Desired frame rate in fps (e.g. 15)
                  Default is 30
-l                List all devices found then exit
-h                Show help
```

If a camera does not support the default parameters then it can be supported using
the correct command line options. Some examples:

* PureThermal Mini Pro Flir Lepton: ```voxl-uvc-server -r 160x120 -f 9```
* Flir Boson 640: ```voxl-uvc-server -r 640x512```

Note: The Flir Boson 640 will always run at 60fps even if you request a different rate

## Helpful tips

There are a few nice debugging tools available to help trouble shoot issues and / or
determine the required configuration for a new camera.

The Linux command line utility ```lsusb``` can be very helpful to see what devices
are on the USB bus. One can run the command without the USB camera attached, then
attach it and see if it appears in the list. This will provide you with the USB vendor
and product id of the camera. For example:

```
yocto:/home/root# lsusb
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
yocto:/home/root# lsusb
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 002: ID 046d:0825 Logitech, Inc. Webcam C270
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

This shows the Logitech C270 camera with USB vendor id 0x046d and product id 0x0825.

```lsusb -v -d <vid:pid>``` can also provide a lot more information about a specific device. For
example:

```
yocto:/home/root# lsusb -v -d 046d:0825

Bus 001 Device 002: ID 046d:0825 Logitech, Inc. Webcam C270
Device Descriptor:
  bLength                18
  bDescriptorType         1
  bcdUSB               2.00
  bDeviceClass          239 Miscellaneous Device
  bDeviceSubClass         2
  bDeviceProtocol         1 Interface Association
  bMaxPacketSize0        64
  idVendor           0x046d Logitech, Inc.
  idProduct          0x0825 Webcam C270
  bcdDevice            0.12
  iManufacturer           0
  iProduct                0
  iSerial                 2 9C06CEC0
  bNumConfigurations      1
... etc. etc. ...
```

A utility script named ```show-video-device-info.sh``` is provided as part of the
voxl-uvc-server package. It can also be used to get some useful information about
any attached UVC cameras. For example:

```
yocto:/home/root# show-video-device-info.sh
/dev/video2: UVC Camera (046d:0825) 046d:0825
```

Running voxl-uvc-server with the ```-l``` option will dump some diagnostic information about
all of the cameras discovered and then exit.

Running voxl-uvc-server with the ```-d``` and / or ```-v``` options will cause diagnostic
messages to be output as the server is running.

## USB Host Power Issues on VOXL 1 and VOXL Flight

__Applicable only to VOXL 1 and VOXL Flight__

Some USB cameras require more power than can be provided by the USB bus and will
fail to operate without a separate power source that can provide the needed current. An
example of this is the Flir Boson 640. It can operate using a USB 2.0 cable attached to the
Voxl adb port, but that is usually not desirable since that port is needed for ```adb```. Attaching it
to a USB connector on an add-on board will fail. However, the ModalAI M0078
debug board provides additional power to the USB port and can support the Flir Boson 640.

## Running Two USB Cameras Simultaneously

Due to a bug in libuvc, to run two USB cameras simultaneously, you need to make one camera have different formatting than the other camera. The most non-intrusive way to do this is to set the frame rate of one camera different than the other.

Here we set the frame rate of the first camera to 25 frames per second using the ```-f``` option:

```voxl-uvc-server -f 25```

Assuming the second camera defults to 30 frames per second, you do not need to manually set it's frame rate.

```voxl-uvc-server```

## Source

Source code is [here](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-uvc-server/-/tree/dev).
