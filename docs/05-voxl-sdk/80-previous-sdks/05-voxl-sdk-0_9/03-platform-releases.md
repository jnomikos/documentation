---
layout: default
title: Software Releases 0.9
parent: VOXL SDK 0.9
search_exclude: true
nav_order: 3
has_children: true
has_toc: false
permalink: /platform-releases-0_9/
---

# Platform Releases
{: .no_toc }

## Table of contents
{: .no_toc }

1. TOC
{:toc}

## Overview

Software for VOXL compute platforms is distributed in bundles called Platform Releases which consist of a system image, [VOXL Suite](/voxl-suite/), and an installer.

The system image contains the basic operating system, kernel and various system partitions to make VOXL boot. [VOXL Suite](/voxl-suite/) is the collection of core ModalAI tools and services that run on VOXL. These two are often interdependent and as such are tested and released together as a platform release and should be used together.


## Platform Names: APQ8096 & QRB5165

Software Packages are often unique to the Qualcomm chipset used on a paritcular board as opposed to particular VOXL PCB design.

### 1st generation: APQ8096

[VOXL](/voxl/) and [VOXL-Flight](/voxl-flight/) both use the APQ8096 chipset and share software and Platform Releases.

### 2nd generation: QRB5165

The second generation [VOXL 2](/voxl2/) and [VOXL 2 Mini](/voxl2-mini/) autopilot and compute platform is based on the QRB5165 chipset. This is the same chipset as the [Qualcomm Flight RB5](/qualcommflightrb5/) and so it shares [VOXL Suite](/voxl-suite/) packages but has a slightly different system image to VOXL2 due to hardware differences.


## Install

### 1 Where To Download

Platform releases can be found on our [Protected Downloads Page](https://developer.modalai.com/asset/2)

* [VOXL 1 (APQ8096) Platform Releases](https://developer.modalai.com/asset/1)
* [VOXL 2 (QRB5165) Platform Releases](https://developer.modalai.com/asset/2)
* [VOXL 2 Mini (QRB5165) Platform Releases](https://developer.modalai.com/asset/10)
* [RB5 Flight with VOXL SDK Platform Releases](https://developer.modalai.com/asset/3)

### 2 Install ADB on Host Computer

Follow the instructions on the [Setup ADB page](/setting-up-adb/).

### 3 Power up VOXL and Connect via USB

It is recommended to use a USBC to USBA cable.

We have seen issues with USBC to USBC cables on some host machines.

### 4 Run Install Script

**NOTE:** The following commands are all ran on the host computer, not on VOXL.

- Unzip the download, in this example we'll assume the download name was `voxl2_platform_M.m.b.tar.gz` where M.m.b is the version of the SDK.

```bash
❯ tar -xzvf voxl2_platform_M.m.b.tar.gz
```

- Get ready to run the script by going into the directory you just unzipped

```bash
❯ cd voxl2_platform_M.m.b
```

- Run the following:

```bash
./install.sh
```

- Depending on release version, you may be prompted to preserve or wipe the /data/ parition. When in doubt, preserve (DO NOT WIPE) the data partition if you don't explicitly intend to. It contains calibration and factory configuration data that should be preserved between platform release upgrades.

- If everything flashed successfully, you should see a message at the end showing how to configure the drone:
```
=====================================================
|     Done installing voxl-suite for QRB5165        |
|                                                   |
|         Please visit our online guides at         |
|             https://docs.modalai.com/             |
|                                                   |
|      To configure Modal Pipe Architecture (MPA)   |
|       services, please run voxl-configure-mpa     |
|                                                   |
| To see what MPA services are enabled and running, |
|           please run voxl-inspect-services        |
|                                                   |
|   To see a list of MPA utilities available, adb   |
|      or ssh into VOXL and type voxl{TAB}{TAB}     |
=====================================================
```

### 5 Configure Software

After flashing a Platform Release, your VOXL `/data/` parition will still contain IMU and Camera calibration from before. However, the system parition is now back to a clean state with no services enabled. Most likely you will want to start enabling individual services manually or use the [voxl-configure-mpa](/voxl-configure-mpa/) tool to set up all services at once and work from there. If you purchased a ModalAI [dev drone](/voxl-dev-drones/) then the model number for that drone will be saved in the preserved `/data/` parition between platform release flashes enabling you to configure all VOXL services to a standard configuration for that dev drone with the factory reset option:

```bash
voxl-configure-mpa --factory-reset
```

Follow the guides in the [Software Configuration Page](/software-configuration/) for more information on configuring and using VOXL Services.

#### Factory Production Configuration

[voxl-configure-mpa](/voxl-configure-mpa/) supports production factory configuration with pre-configured scenarios based on ModalAI's products. 

The following video shows a clean installation and production configuration for a VOXL Deck (MDK-F0002) that contains a "legacy" / green VOXL unit.

<div class="video-container">
    <iframe src="https://www.youtube.com/embed/_wMElWgiWBs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

The following video shows a clean installation and production configuration for a [VOXL Flight Deck](/flight-deck/) (MDK-F0001) that contains an [M0019 VOXL Flight](/voxl-flight/). This video demonstrates a clean flash getting all of the way to the [stereo calibration](/voxl-camera-calibration/) page.

<div class="video-container">
    <iframe src="https://www.youtube.com/embed/m2rEVyb2AHo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

## Post Tweaks

### Overview
To save time configuring multiple drones, you are encouraged to add your own configuration steps to a platform release via the Post Tweaks method to automatically perform your customizations as part of the flashing process. This is a good place to add calls to (voxl-configure-cameras)[/configure-cameras/] and [voxl-configure-extrinsics](/voxl-configure-extrinsics/) for your custom camera config for exmaple.

Inside of each platform release tar archive is a folder called `post-tweaks`. In this folder you can add a bash script called `apply-tweaks.sh` containing any extra setup and configuration steps you may want run automatically. This script will be executed from within the `post-tweaks` directory by the top-level install.sh script after the normal platform release installation. Depending on the platform release you are trying to install, this script may already exist and contain something from ModalAI.

You can also add files to this folder and `adb push` them to VOXL in the `apply-tweaks.sh` script.

### Example

Example `apply-tweaks.sh` script to reconfigure all services to defaults for the Flight Deck, disable VIO, and then enable DFS server for VOA.

```
#!/bin/bash

adb shell "rsync -r /share/qualcomm-proprietary/usr/ /usr"

adb shell "voxl-configure-mpa -p -f flight-deck"

echo
echo "Configuring DFS"
adb shell "voxl-configure-dfs factory_enable"
adb shell "voxl-configure-qvio disable"

echo
echo "Rebooting"
adb reboot
adb wait-for-device

echo "Done"
```

