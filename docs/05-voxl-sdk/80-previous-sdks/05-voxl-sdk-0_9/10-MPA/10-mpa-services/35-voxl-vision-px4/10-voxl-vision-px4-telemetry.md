---
layout: default
title: Mavlink Telemetry Routing 0.9
parent: VOXL Vision PX4 0.9
search_exclude: true
nav_order: 10
permalink: /voxl-vision-px4-telemetry-0_9/
---

# VOXL Vision PX4 Mavlink Telemetry Routing
{: .no_toc }

voxl-vision-px4 serves as a telemetry proxy between WiFi/LTE/Microhard (UDP) and PX4's UART telemetry port. This is always enabled and removes the need to use a USB cable or Serial Radio Link for telemetry data.

Connections between VOXL and qGroundControl can be configured either by informing voxl-vision-px4 of QGC's IP address, or visa-versa. UDP connections can also be made locally on VOXL allowing services like MAVROS and MAVSDK to run on VOXL alongside voxl-vision-px4 and still communicate with the PX4 autopilot.

All mavlink communication with PX4 is ultimately done over a 1Mbps UART connection and the mavlink packet parsing & error checking is accelerated & buffered by VOXL's sDSP for ultra-efficient handling of large data rates. Up to 16 simultaneous UDP connection are supported at once!


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![px4_chart-architecture](/images/voxl-sdk/voxl-vision-px4/px4_chart-architecture.png)


## External UDP Mavlink Connections

Up to 16 simultaneous external UDP connections can be established with voxl-vision-px4. These can be connections to qGroundControl, MAVROS, MAVSDK, or any custom software on the same network as VOXL. voxl-vision-px4 listens for mavlink heartbeats on the standard mavlink port 14550 and opens a new link when it receives a heartbeat from a new IP. Each link receives all mavlink data coming from PX4 and any messages received on the UDP port will be forwarded to PX4.

VOXL can be configured to initialize the connection with a known ground station IP, or qGroundControl can be told the IP address of VOXL in the "Comm Links" settings section. The following two sections describe these two methods.


### Initiating a Connection from QGC

Follow this video tutorial to see how:

<div class="video-container">
    <iframe src="https://www.youtube.com/embed/gaV4ubIYFzs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>



### Manully Configuring QGC IP Address

Manually inform voxl-vision-px4 of the IP address of your QGC Ground Controller by providing the IP address of your ground station computer in the "qgc_ip" field of the config file. This field is also configured as one of the first questions in the configuration wizard.

If you wish to rely on QGC to initiate the connection to VOXL you can leave this field as-is, blank, or "NULL".


```json
    "qgc_ip":                   "192.168.8.60",
```

A second manual IP entry can optionally be configured and enabled. Both of these will fields will tell voxl-vision-px4 to continuously forward all mavlink traffic from PX4 to these IP addresses at port 14550 which is the default UDP port for qGroundControl.

```json
    "en_secondary_qgc": false,
    "secondary_qgc_ip": "192.168.1.214",
```

Do not try to set either of these IP address fields to the localhost address "127.0.0.1". Use the `"en_localhost_mavlink_udp"` feature on port 14551 instead.



## Localhost UDP Mavlink Communication

If you are running MAVROS/MAVSDK onboard VOXL and wish to open access to PX4 through a localhost UDP port simply ensure the follow feature is enabled. This is set to `true` by default.

```json
    "en_localhost_mavlink_udp": true,
```

This will allow one local process to communicate with PX4 via port 14551, NOT 14550 which is reserved for connections outside the board. These separation prevents conflicts between the two sockets. Both MAVROS and MAVSDK can be configured to use this port.

For more information, check out our [MAVROS example](/mavros/).


## Next

Next page: [Flying with VIO](/flying-with-vio/)
