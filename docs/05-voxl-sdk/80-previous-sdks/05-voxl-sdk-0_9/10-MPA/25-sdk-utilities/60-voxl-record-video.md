---
layout: default
title: VOXL Record Video 0.9
parent: SDK Utilities 0.9
search_exclude: true
nav_order: 60
has_children: true
has_toc: true
permalink: /voxl-record-video-0_9/
---

# Record Video

NOTE: This is only enabled (by default) on hires cameras. The name of the pipe this encoded data is being sent to is: `hires_record` and can only be used once voxl-camera-server has been ran.

Like streaming low latency over h264 at 720p/4K, there is also the capability now of doing the same thing via this record feature. This once again, has the ability to compress image frames using OMX for a no-copy hardware accelerated and memory optimized data path to publish this compressed and encoded data. This encoded video is then saved on disk using this record feature. The following parameters are how a user can change the resolution and bitrate in voxl-camara server:

1. `record_width`<br />
2. `record_height`<br />
3. `record_bitrate`<br />

In order to save the video now, (Only supported on SDK 0.9.5 or higher) the user can open a terminal window on the voxl2 and type the following `voxl-record-video hires_record -o DIRECTORY/OF/OUTPUTVIDEO.h264`. To stop recording, in the same terminal window, simply type `ctrl + c`.

For VOXL 2, there are currently only a subset of three (3) resolutions that work with OMX and the HIRES_RECORD pipe:

1. 4096x2160<br />
2. 2048x1536<br />
3. 1024x768 <br />

[Source Code](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/blob/master/tools/voxl-record-video.c)
