---
layout: default
title: VOXL Modem 0.9
parent: SDK Utilities 0.9
search_exclude: true
nav_order: 79
permalink: /voxl-modem-0_9/
---

# VOXL Modem
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

[voxl-modem](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem) is a tool which enables modem usage for the following hardware:

- ModalAI LTE v1 (WNC CSS Modules, apq8096 only)
- ModalAI LTE v2 (Sierra Modules)
- 5G (Quectel)
- Doodle Labs (Helix Radio)
- Microhard

## Installation

voxl-modem is installed via ipk package.

```
opkg update

opkg install voxl-modem
```

When using modem related hardware it is recommended to be on the latest [platform release](https://docs.modalai.com/platform-releases/).

## Running voxl-modem

A one time configuration script, `voxl-configure-modem` is required to do first time setup. This can be ran via. the command line on any VOXL product:

```
voxl2:/$ voxl-configure-modem

Making new config file /etc/modalai/voxl-modem.conf
 
What type of modem are you using?

If you are unsure of which modem you have, take a look at the following datasheets:

v2 LTE Modem: https://docs.modalai.com/lte-modem-and-usb-add-on-v2-datasheet/
Microhard Modem: https://docs.modalai.com/microhard-add-on-datasheet/

1) v2
2) microhard
3) doodle
4) quectel
```

This script will take you through a number of questions in order to setup your modem hardware and enable the voxl-modem systemd service. 

Depending on which VOXL hardware you're using the available options may vary.

## User Guides and Datasheets

|   | User Guide  | Datasheet  |
|---|---|---|
| ModalAI LTE v1 | [LTE v1 User Guide](https://docs.modalai.com/lte-v1-modem-user-guide/) | [LTE v1 Datasheet](https://docs.modalai.com/lte-modem-and-usb-add-on-datasheet/)  |
| ModalAI LTE v2 | [LTE v2 User Guide](https://docs.modalai.com/lte-v2-modem-user-guide/) | [LTE v2 Datasheet](https://docs.modalai.com/lte-modem-and-usb-add-on-v2-datasheet/) |
| 5G (Quectel) | [5G User Guide](https://docs.modalai.com/sentinel-user-guide-connect-network/#connecting-to-5g-network)  | [M0090 Datasheet](https://docs.modalai.com/5G-Modem-datasheet/#5g-modem-add-on-datasheet)  |
| Doodle Labs | [Doodle Labs User Guide](https://docs.modalai.com/doodle-labs-user-guide/)  |  [Doodle Labs Datasheet](https://doodlelabs.com/datasheets/smart-radio-platform-1625-2510-mhz-datasheet/) |
| Microhard | [Microhard User Guide](https://docs.modalai.com/microhard-add-on-manual/)  | [Microhard Datasheet](https://docs.modalai.com/microhard-add-on-datasheet/)  |
