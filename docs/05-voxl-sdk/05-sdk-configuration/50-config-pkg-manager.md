---
title: Configure Package Manager
layout: default
parent: SDK Configuration
has_children: true
nav_order: 50
permalink: /configure-pkg-manager/
---

# How to Configure the VOXL Package Manager
{: .no_toc }

## Table of contents
{: .no_toc }

1. TOC
{:toc}

## Overview

The VOXL SDK is distributed as both a pre-built [installer](/flash-system-image/#flashing-a-voxl-sdk-release) and as a publicly accessible [Debian Package Repo Here](http://voxl-packages.modalai.com/dists/). After flashing your VOXL with an SDK installer, VOXL will be configured to point its package manager to the correct Debian Package Repo online to allow for updates.

This page describes how to use the `voxl-configure-pkg-manager` script to configure the package manager to pull from a particular release section.

Generally you do NOT need to use this tool unless you know what you are doing and have a good reason to change from a stable SDK distribution.



## Available Sections

All ModalAI package repositories are available at [http://voxl-packages.modalai.com/dists/](http://voxl-packages.modalai.com/dists/) allowing opkg or apt to install and update packages using an internet connection.

### sdk-x.y

We will periodically assemble all of our packages into a tested and stable SDK release. These are enumerated to match the version of [voxl-suite](/voxl-suite/) within (i.e. SDK-0.8 has voxl-suite version 0.8 and its dependencies). These releases are considered stable and receive new updates and features that should not break APIs.

An SDK minor version release is tied to a system image version so you cannot upgrade to a newer SDK with the voxl-configure-package-manager tool. You must follow the [SDK installer instructions](/flash-system-image/#flashing-a-voxl-sdk-release).


### Staging

This is the default-enabled repository on nightly SDK builds and consists of packages being tested and staged for stable release. These are experimental packages with features that will appear in future stable SDK releases.

### Dev

This is the development repository containing often untested or experimental software that is automatically pushed via CI on every commit made by ModalAI's software team. This repository serves for ModalAI to use internally. Packages on this repo are updated very regularly and are not guaranteed to function.


## Keeping updated

You can easily check to see if any updated packages are available to pull by running `opkg update && opkg upgrade` on VOXL 1 or `apt update && apt upgrade` on VOXL 2. Especially if you decide to use the staging repo, new packages will often be available weekly if not daily.


### Additional Note on APT:
{: .no_toc }

VOXL2/RB5F are built on a standard Ubuntu 18 image, so we recommend that you run `apt update && apt upgrade` regularly on these platforms no matter what as there will often be updates to the standard Ubuntu packages even if there haven't been to ours.

# Adding the Modal AI Package Repository

Some VOXL SDK packages can be installed on a development machine as well as the VOXL platform. In order to install these packages you will need a Debian-based operating system (such as Ubuntu) and you will need to configure the `voxl-packages` package repository. This can either be configured manually or, if you are on Ubuntu, using the Ubuntu Software Manager. 

The package repository can be configured to use packages from a particular SDK release or to use the staging packages to constantly get the latest updates.

## Ubuntu Software Manager

![ubuntu-software](/images/voxl-sdk/ubuntu-software.png)

Open the "Software and Updates" app on Ubuntu and navigate to the "Other Software" tab. Press the "Add" button and use the following as the APT line:

```
deb http://voxl-packages.modalai.com ./dists/qrb5165/staging/binary-arm64/
```

Replace "staging" with "sdk-x.y" if you prefer to use packages from a specific SDK release instead.

## Manual Configuration

Place the APT line in a new file in the `/etc/apt/sources.list.d/` directory, e.g.:

```
echo "deb http://voxl-packages.modalai.com ./dists/qrb5165/staging/binary-arm64/" | sudo tee /etc/apt/sources.list.d/voxl.list
```

Replace "staging" with "sdk-x.y" if you prefer to use packages from a specific SDK release instead.